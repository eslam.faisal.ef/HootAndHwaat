package com.fekrah.hootandhwaat.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.facebook.drawee.view.SimpleDraweeView;
import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.helper.SharedHelper;
import com.fekrah.hootandhwaat.models.City;
import com.fekrah.hootandhwaat.models.Errors;
import com.fekrah.hootandhwaat.models.User;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fekrah.hootandhwaat.activities.LoginChooseActivity.LOGIN_TYPE;

public class RegisterActivity extends AppCompatActivity {


    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.pass)
    EditText pass;

    @BindView(R.id.address)
    EditText address;

    @BindView(R.id.phone)
    EditText phone;

    @BindView(R.id.address_view)
    View addressView;

    @BindView(R.id.location_view)
    View locationView;
    private String loginType;
    private Apis apis;

    private String REQUEST_FOR_PICTURE;
    private static final String PROFILE_IMAGE_REQUEST = "PROFILE_IMAGE_REQUEST";
    private Bitmap thumbBitmap = null;
    UCrop.Options options;
    byte[] profilebyte;
    private Uri imageUri;

    @BindView(R.id.user_image)
    SimpleDraweeView tripImage;

    List<String> cities = new ArrayList<>();
    List<String> citiesId = new ArrayList<>();

    public static String CITY_ID ;
    private List<City> citiesBody;

    @BindView(R.id.cities_spinner)
    Spinner citiesSpinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        loginType = intent.getStringExtra(LOGIN_TYPE);
        options = new UCrop.Options();
        options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));


        apis = BaseClient.getBaseClient().create(Apis.class);


        tripImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                REQUEST_FOR_PICTURE = PROFILE_IMAGE_REQUEST;
                ImagePicker.create(RegisterActivity.this)
                        .limit(1)
                        .theme(R.style.UCrop)
                        .folderMode(true)
                        .start();
            }
        });
        if (loginType.equals(LoginChooseActivity.CUSTOMER)){
            locationView.setVisibility(View.GONE);
        }else {
            addressView.setVisibility(View.GONE);
        }
        getcities(apis);
    }


    private void getcities(Apis api) {
        if (cities!=null&&cities.size()>1){
            cities.clear();
        }
        Call<List<City>> getCiriesCall = api.getCities();

        getCiriesCall.enqueue(new Callback<List<City>>() {
            @Override
            public void onResponse(Call<List<City>> call, Response<List<City>> response) {
                if (response.body() != null) {
                    if (response.body().size() > 0) {

                        citiesBody = response.body();
                        for (City city : citiesBody) {
                            cities.add(city.getC_name());
                            citiesId.add(city.getC_id());
                        }
                        ArrayAdapter adapter = new ArrayAdapter(RegisterActivity.this, R.layout.layout_cities_spinner_item, cities);

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        citiesSpinner.setAdapter(adapter);
                        citiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                CITY_ID = String.valueOf(citiesBody.get(position).getC_id());
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                }
                findViewById(R.id.wave_view).setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<City>> call, Throwable t) {

            }
        });
    }


    public void goMain(View view) {

        if (loginType.equals(LoginChooseActivity.CUSTOMER))
            registerClient();
        else
            registerProvider();
        //  startActivity(new Intent(this,MainActivity.class));
    }

    private void registerProvider() {
        if (name.getText().toString().equals("") ||
                email.getText().toString().equals("") ||
                phone.getText().toString().equals("") ||
                pass.getText().toString().equals("") ||
                imageUri == null) {
            Toast.makeText(this, getString(R.string.fill_all), Toast.LENGTH_SHORT).show();
            return;
        }
        File file2 = new File(imageUri.getPath());
        RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file2);
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", String.valueOf(System.currentTimeMillis()+ ".jpg"), surveyBody);

        RequestBody name2 = RequestBody.create(MediaType.parse("text/plain"), name.getText().toString());
        RequestBody email2 = RequestBody.create(MediaType.parse("text/plain"), email.getText().toString());
        RequestBody phone2= RequestBody.create(MediaType.parse("text/plain"), phone.getText().toString());
        RequestBody address2 = RequestBody.create(MediaType.parse("text/plain"), address.getText().toString());
        RequestBody pass2 = RequestBody.create(MediaType.parse("text/plain"), pass.getText().toString());
        RequestBody boat = RequestBody.create(MediaType.parse("text/plain"), pass.getText().toString());

        apis.registerProvider(
                email2,
                pass2,
                name2,
                phone2,
                image,
                Integer.parseInt(CITY_ID),
                boat
        ).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.body() != null) {
                    User user = response.body();
                    if (user.getUser() != null) {

                        SharedHelper.putKey(getApplicationContext(), "log_in", "yes");
                        SharedHelper.putKey(getApplicationContext(), "log_in_type", "userProvider");
                        SharedHelper.putKey(getApplicationContext(), "user_name", user.getUser().getName());
                        SharedHelper.putKey(getApplicationContext(), "user_address", user.getUser().getAddress());
                        SharedHelper.putKey(getApplicationContext(), "user_email", user.getUser().getEmail());
                        SharedHelper.putKey(getApplicationContext(), "user_image", user.getUser().getImage());
                        SharedHelper.putKey(getApplicationContext(), "user_token", user.getUser().getToken());
                        SharedHelper.putKey(getApplicationContext(), "user_mobile", user.getUser().getMobile());
                        SharedHelper.putKey(getApplicationContext(), "user_id", user.getUser().getCustomer_id());
                        Log.d("llllll", "onResponse: " + user.getUser().getUser_id());

                        MainActivity.hideClient();

                        finish();

                    } else {
                        Toast.makeText(RegisterActivity.this, "This email is exist use another one", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(RegisterActivity.this, "Wrong data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, "Some thing went wrong", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void registerClient() {
        if (name.getText().toString().equals("") ||
                email.getText().toString().equals("") ||
                phone.getText().toString().equals("") ||
                pass.getText().toString().equals("") ||
                address.getText().toString().equals("") ||
                imageUri == null) {
            Toast.makeText(this, getString(R.string.fill_all), Toast.LENGTH_SHORT).show();
            return;
        }
        File file2 = new File(imageUri.getPath());
        RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file2);
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", String.valueOf(System.currentTimeMillis()+ ".jpg"), surveyBody);

        RequestBody name2 = RequestBody.create(MediaType.parse("text/plain"), name.getText().toString());
        RequestBody email2 = RequestBody.create(MediaType.parse("text/plain"), email.getText().toString());
        RequestBody phone2= RequestBody.create(MediaType.parse("text/plain"), phone.getText().toString());
        RequestBody address2 = RequestBody.create(MediaType.parse("text/plain"), address.getText().toString());
        RequestBody pass2 = RequestBody.create(MediaType.parse("text/plain"), pass.getText().toString());


        apis.registerClient(
                email2,
                pass2,
                address2,
                name2,
                phone2,
                image
        ).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.body() != null) {
                    User user = response.body();
                    if (user.getUser() != null) {

                        SharedHelper.putKey(getApplicationContext(), "log_in", "yes");
                        SharedHelper.putKey(getApplicationContext(), "log_in_type", "userClient");
                        SharedHelper.putKey(getApplicationContext(), "user_name", user.getUser().getName());
                        SharedHelper.putKey(getApplicationContext(), "user_address", user.getUser().getAddress());
                        SharedHelper.putKey(getApplicationContext(), "user_email", user.getUser().getEmail());
                        SharedHelper.putKey(getApplicationContext(), "user_image", user.getUser().getImage());
                        SharedHelper.putKey(getApplicationContext(), "user_token", user.getUser().getToken());
                        SharedHelper.putKey(getApplicationContext(), "user_mobile", user.getUser().getMobile());
                        SharedHelper.putKey(getApplicationContext(), "user_id", user.getUser().getUser_id());
                        MainActivity.hideProvider();

                        finish();
                    } else {
                        Toast.makeText(RegisterActivity.this, user.getError().get(0).getMsg(), Toast.LENGTH_LONG).show();
                        for (Errors errors : user.getError()){
                            Log.d("eeeeeee", "on error: "+errors.getMsg());
                        }

                    }

                } else {
                    Toast.makeText(RegisterActivity.this, "Wrong data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, "Some thing went wrong", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            //Toast.makeText(this, "", Toast.LENGTH_LONG).show();
            return;
        }
        String destinationFileName = "SAMPLE_CROPPED_IMAGE_NAME" + ".jpg";

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {

            Image image = ImagePicker.getFirstImageOrNull(data);

            Uri res_url = Uri.fromFile(new File((image.getPath())));
            //imageUri = image.getPath();
            CropImage(image, res_url);

        } else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            //  if (resultUri!=null)
            assert resultUri != null;
            bitmapCompress(resultUri);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            thumbBitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);

            profilebyte = byteArrayOutputStream.toByteArray();
            tripImage.setImageURI(resultUri);
            tripImage.setVisibility(View.VISIBLE);


            imageUri = resultUri;

        }

    }

    private void CropImage(Image image, Uri res_url) {
        UCrop.of(res_url, Uri.fromFile(new File(this.getCacheDir(), image.getName())))
                .withOptions(options)
                .start(this);
    }

    private void bitmapCompress(Uri resultUri) {
        final File thumbFilepathUri = new File(resultUri.getPath());

        try {
            thumbBitmap = new Compressor(this)
                    .setQuality(50)
                    .compressToBitmap(thumbFilepathUri);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
