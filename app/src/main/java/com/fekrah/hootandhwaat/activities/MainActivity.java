package com.fekrah.hootandhwaat.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.fragments.CallUsFragment;
import com.fekrah.hootandhwaat.fragments.ClientOrdersFragment;
import com.fekrah.hootandhwaat.fragments.CreateAdFragment;
import com.fekrah.hootandhwaat.fragments.MainFragment;
import com.fekrah.hootandhwaat.fragments.ProfileFragment;
import com.fekrah.hootandhwaat.fragments.TermsFragment;
import com.fekrah.hootandhwaat.fragments.VehiclesFragment;
import com.fekrah.hootandhwaat.fragments.providerAds.MyAdsFragment;
import com.fekrah.hootandhwaat.fragments.client_orders.ProviderFishesOrdersFragment;
import com.fekrah.hootandhwaat.fragments.client_orders.ProviderTripsOrdersFragment;
import com.fekrah.hootandhwaat.fragments.provider_fishes_orders.NewProviderFishesOrdersFragment;
import com.fekrah.hootandhwaat.fragments.provider_trips_orders.NewProviderTripsOrdersFragment;
import com.fekrah.hootandhwaat.helper.SharedHelper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FragmentManager fragmentManager = getSupportFragmentManager();
    MainFragment mainFragment = new MainFragment();
    ClientOrdersFragment myOrdersFragment = new ClientOrdersFragment();
    ProfileFragment profileFragment = new ProfileFragment();
    CallUsFragment callUsFragment = new CallUsFragment();
    TermsFragment termsFragment = new TermsFragment();
    CreateAdFragment createAdFragment = new CreateAdFragment();
    MyAdsFragment myAdsFragment = new MyAdsFragment();
    VehiclesFragment vehiclesFragment = new VehiclesFragment();
    NewProviderTripsOrdersFragment providerTripsOrdersFragment = new NewProviderTripsOrdersFragment();
    NewProviderFishesOrdersFragment providerFishesOrdersFragment = new NewProviderFishesOrdersFragment();

    ProviderTripsOrdersFragment clientTripsOrdersFragment = new ProviderTripsOrdersFragment();
    ProviderFishesOrdersFragment clientFishesOrdersFragment = new ProviderFishesOrdersFragment();

    public static NavigationView navigationView;
    private SimpleDraweeView profileImage;
    private TextView profileName;
    private TextView mainSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("ooooooo", "onCreate: " + SharedHelper.getKey(this, "user_id"));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        toggle.setDrawerSlideAnimationEnabled(true);
        fragmentManager.beginTransaction().add(R.id.container, mainFragment).commit();

        navigationView.setCheckedItem(R.id.nav_main);
        View header = navigationView.getHeaderView(0);
        profileImage = header.findViewById(R.id.profile_image);
        profileName = header.findViewById(R.id.profile_name);
        mainSignIn = header.findViewById(R.id.main_sign_in);
        mainSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, LoginChooseActivity.class));
            }
        });
        hideAll();

        hideUserInfo();
        //navigationView.getMenu().getItem(0).setVisible(false);
        if (SharedHelper.getKey(getApplicationContext(), "log_in").equals("yes")) {
            if (SharedHelper.getKey(getApplicationContext(), "log_in_type").equals("userProvider")) {
                hideClient();
                getUserInfo();
            } else {
                hideProvider();
                getUserInfo();
            }
        }

        Log.d("oooooo", "onCreate: " + SharedHelper.getKey(getApplicationContext(), "user_id"));


    }

    private void getUserInfo() {
        profileImage.setVisibility(View.VISIBLE);
        profileName.setVisibility(View.VISIBLE);
        profileImage.setImageURI(SharedHelper.getKey(this, "user_image"));
        profileName.setText(SharedHelper.getKey(this, "user_name"));
        mainSignIn.setVisibility(View.GONE);
    }

    public void hideUserInfo() {
        profileImage.setVisibility(View.GONE);
        profileName.setVisibility(View.GONE);
        mainSignIn.setVisibility(View.VISIBLE);
    }

    public static void hideClient() {
        navigationView.getMenu().getItem(1).setVisible(false);
        navigationView.getMenu().getItem(3).setVisible(true);
        navigationView.getMenu().getItem(2).setVisible(true);
        navigationView.getMenu().getItem(4).setVisible(true);
        navigationView.getMenu().getItem(5).setVisible(true);
        navigationView.getMenu().getItem(6).setVisible(false);
        navigationView.getMenu().getItem(7).setVisible(false);
        navigationView.getMenu().getItem(11).setVisible(true);
        navigationView.getMenu().getItem(8).setVisible(true);

    }

    public static void hideProvider() {
        navigationView.getMenu().getItem(1).setVisible(false);
        navigationView.getMenu().getItem(3).setVisible(false);
        navigationView.getMenu().getItem(2).setVisible(false);
        navigationView.getMenu().getItem(4).setVisible(false);
        navigationView.getMenu().getItem(5).setVisible(false);
        navigationView.getMenu().getItem(6).setVisible(true);
        navigationView.getMenu().getItem(7).setVisible(true);
        navigationView.getMenu().getItem(11).setVisible(true);
        navigationView.getMenu().getItem(8).setVisible(true);
    }

    public void hideAll() {
        navigationView.getMenu().getItem(1).setVisible(false);
        navigationView.getMenu().getItem(3).setVisible(false);
        navigationView.getMenu().getItem(2).setVisible(false);
        navigationView.getMenu().getItem(4).setVisible(false);
        navigationView.getMenu().getItem(5).setVisible(false);
        navigationView.getMenu().getItem(6).setVisible(false);
        navigationView.getMenu().getItem(7).setVisible(false);
        navigationView.getMenu().getItem(8).setVisible(false);
        navigationView.getMenu().getItem(11).setVisible(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideAll();
        hideUserInfo();
        if (SharedHelper.getKey(getApplicationContext(), "log_in").equals("yes")) {
            if (SharedHelper.getKey(getApplicationContext(), "log_in_type").equals("userProvider")) {
                hideClient();
                getUserInfo();
            } else {
                hideProvider();
                getUserInfo();
            }
        }

    }

    MenuItem add_vehicles_item;
    MenuItem add_ad;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);


        add_vehicles_item = menu.findItem(R.id.add_vehicles);
        add_ad = menu.findItem(R.id.add_ads);

        if (getTitle().equals(getString(R.string.my_vehicles))) {
            add_vehicles_item.setVisible(true);
        } else {
            add_vehicles_item.setVisible(false);
        }

        if (getTitle().equals(getString(R.string.my_ads))) {
            add_ad.setVisible(true);
        } else {
            add_ad.setVisible(false);
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add_ads) {
            startActivity(new Intent(this, CreateAdActivity.class));
        } else if (id == R.id.add_vehicles) {
            startActivity(new Intent(this, AddVehiclesActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_main) {
            fragmentManager.beginTransaction().replace(R.id.container, mainFragment).commit();
            setTitle(R.string.main_page);
            invalidateOptionsMenu();
        } else if (id == R.id.nav_my_orders) {
            fragmentManager.beginTransaction().replace(R.id.container, myOrdersFragment).commit();
            setTitle(R.string.my_orders);
            invalidateOptionsMenu();

        } else if (id == R.id.nav_profile) {
            fragmentManager.beginTransaction().replace(R.id.container, profileFragment).commit();
            setTitle(R.string.profile);
            invalidateOptionsMenu();

        } else if (id == R.id.nav_call_us) {
            fragmentManager.beginTransaction().replace(R.id.container, callUsFragment).commit();
            setTitle(R.string.cal_us);
            invalidateOptionsMenu();

        } else if (id == R.id.nav_vehicle) {
            fragmentManager.beginTransaction().replace(R.id.container, vehiclesFragment).commit();
            setTitle(R.string.my_vehicles);
            invalidateOptionsMenu();

        } else if (id == R.id.nav_terms) {
            fragmentManager.beginTransaction().replace(R.id.container, termsFragment).commit();
            setTitle(R.string.terms);
            invalidateOptionsMenu();

//        } else if (id == R.id.nav_create_ad) {
//            fragmentManager.beginTransaction().replace(R.id.container, createAdFragment).commit();
//            setTitle(R.string.create_ad);
//            invalidateOptionsMenu();
        } else if (id == R.id.nav_my_ads) {
            fragmentManager.beginTransaction().replace(R.id.container, myAdsFragment).commit();
            setTitle(R.string.my_ads);
            invalidateOptionsMenu();
        } else if (id == R.id.nav_trips_orders) {
            fragmentManager.beginTransaction().replace(R.id.container, providerTripsOrdersFragment).commit();
            setTitle(R.string.trips_orders);
            invalidateOptionsMenu();
        } else if (id == R.id.nav_fishes_orders) {
            fragmentManager.beginTransaction().replace(R.id.container, providerFishesOrdersFragment).commit();
            setTitle(R.string.fishes_orders);
            invalidateOptionsMenu();
        }else if (id == R.id.nav_client_fishes_orders) {
            fragmentManager.beginTransaction().replace(R.id.container, clientFishesOrdersFragment).commit();
            setTitle(R.string.fishes_orders);
            invalidateOptionsMenu();
        }else if (id == R.id.nav_client_trips_orders) {
            fragmentManager.beginTransaction().replace(R.id.container, clientTripsOrdersFragment).commit();
            setTitle(R.string.trips_orders);
            invalidateOptionsMenu();
        } else if (id == R.id.nav_out) {
            navigationView.setCheckedItem(R.id.nav_main);
            hideUserInfo();
            SharedHelper.clearSharedPreferences(this);
            recreate();
            navigationView.setCheckedItem(R.id.nav_main);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void replace() {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public static void hideKeyBoardFromView(Activity mActivity) {
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = mActivity.getCurrentFocus();
        if (view == null) {
            view = new View(mActivity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
