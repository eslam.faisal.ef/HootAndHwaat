package com.fekrah.hootandhwaat.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.fekrah.hootandhwaat.R;

public class LoginChooseActivity extends AppCompatActivity {

    public static final String CUSTOMER = "customer";
    public static final String PROVIDER = "PROVIDER";
    public static final String LOGIN_TYPE = "login_type";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_choose);
    }

    public void registerCustomer(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(LOGIN_TYPE, CUSTOMER);
        startActivity(intent);
        finish();
    }

    public void registerSailor(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(LOGIN_TYPE, PROVIDER);
        startActivity(intent);
        finish();
    }
}
