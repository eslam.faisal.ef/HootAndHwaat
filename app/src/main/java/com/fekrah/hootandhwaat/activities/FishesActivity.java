package com.fekrah.hootandhwaat.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.adapters.FishesAdapter;
import com.fekrah.hootandhwaat.adapters.FishesTypesAdapter;
import com.fekrah.hootandhwaat.fragments.MainFragment;
import com.fekrah.hootandhwaat.models.fishes.FishAd;
import com.fekrah.hootandhwaat.models.fishes.FishAdData;
import com.fekrah.hootandhwaat.models.fishes.FishesType;
import com.fekrah.hootandhwaat.models.fishes.FishesTypeData;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;
import com.fekrah.hootandhwaat.utility.PaginationScrollListener;
import com.john.waveview.WaveView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fekrah.hootandhwaat.fragments.MainFragment.CITY_ID;

public class FishesActivity extends AppCompatActivity implements FishesTypesAdapter.FisheTypeListener {

    @BindView(R.id.fishes_recyclerview)
    RecyclerView fishesRecyclerView;

    @BindView(R.id.fishes_types_recycler_view)
    RecyclerView fishes_types_recycler_view;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.fish_type_spinner)
    Spinner fishesTypeSpinner;


    List<FishAdData> fishList;
    FishesAdapter adapter;
    LinearLayoutManager fishesLinearLayoutManager;

    List<FishesTypeData> typesDataList = new ArrayList<>();

    FishesTypesAdapter typesAdapter;
    LinearLayoutManager typesLinearLayoutManager;


    private static final int PAGE_START = 1;

    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.
    private int TOTAL_PAGES;
    private int currentPage = PAGE_START;
    private Apis apis;
    private List<FishesTypeData> fishesType;
    private List<String> types = new ArrayList<>();
    private List<String> typesId = new ArrayList<>();
    private int typePosition = -1;
    private String fishTypeID = "1";
    public static String typeName;

    @BindView(R.id.wave_view)
    WaveView progress;

    @BindView(R.id.empty_orders)
    TextView noOrders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trips);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        typesAdapter = new FishesTypesAdapter(typesDataList, this, this);
        typesLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        fishes_types_recycler_view.setLayoutManager(typesLinearLayoutManager);
        fishes_types_recycler_view.setAdapter(typesAdapter);

        initRecyclerView();

        apis = BaseClient.getBaseClient().create(Apis.class);


        getFishesTypes();

    }

    private void initRecyclerView() {
        isLastPage = false;
        isLoading = false;
        currentPage = 1;
        fishList = new ArrayList<>();
        adapter = new FishesAdapter(fishList, this);
        fishesLinearLayoutManager = new LinearLayoutManager(this);
        fishesRecyclerView.setLayoutManager(fishesLinearLayoutManager);
        fishesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        fishesRecyclerView.setAdapter(adapter);


        fishesRecyclerView.addOnScrollListener(new PaginationScrollListener(fishesLinearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPageWithSpecificType();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }


    public void getFishesTypes() {
        progress.setVisibility(View.VISIBLE);
        Apis api = BaseClient.getBaseClient().create(Apis.class);

        Call<FishesType> getCiriesCall = api.getFishesType();

        getCiriesCall.enqueue(new Callback<FishesType>() {
            @Override
            public void onResponse(Call<FishesType> call, Response<FishesType> response) {
                if (response.body() != null && response.body().getTypes().size() >= 1) {

                    fishesType = response.body().getTypes();
                    for (FishesTypeData type : fishesType) {
                        types.add(type.getF_name());
                        typesId.add(type.getF_t_id());
                    }
                    typesAdapter.addAll(fishesType);

                    getFirstWithSpecificType();
                } else {
                    noOrders.setVisibility(View.VISIBLE);
                }
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<FishesType> call, Throwable t) {

                progress.setVisibility(View.GONE);
                noOrders.setVisibility(View.VISIBLE);
            }
        });

    }

    private void getFirstWithSpecificType() {
        adapter.clear();
        progress.setVisibility(View.VISIBLE);

        getFishesAllWithSpecificType().enqueue(new Callback<FishAd>() {
            @Override
            public void onResponse(Call<FishAd> call, Response<FishAd> response) {
                if (response.body() != null && response.body().getData().size() >= 1) {
                    TOTAL_PAGES = response.body().getLast_page();
                    if (response.body() != null) {
                        FishAd fishAd = response.body();
                        adapter.addAll(fishAd.getData());
                    }


                    if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                    else isLastPage = true;


                } else {
                    noOrders.setVisibility(View.VISIBLE);
                }
                progress.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<FishAd> call, Throwable t) {

                progress.setVisibility(View.GONE);
                noOrders.setVisibility(View.VISIBLE);

            }
        });
    }

    private void loadNextPageWithSpecificType() {
        getFishesAllWithSpecificType().enqueue(new Callback<FishAd>() {
            @Override
            public void onResponse(Call<FishAd> call, Response<FishAd> response) {
                adapter.removeLoadingFooter();
                isLoading = false;

                if (response.body() != null) {
                    FishAd fishAd = response.body();
                    adapter.addAll(fishAd.getData());
                }

                if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;

            }

            @Override
            public void onFailure(Call<FishAd> call, Throwable t) {

            }
        });
    }


    private void getFirstAll() {
        getFishesAll().enqueue(new Callback<FishAd>() {
            @Override
            public void onResponse(Call<FishAd> call, Response<FishAd> response) {
                adapter.clear();
                Log.d("llllll", "onResponse: " + response.body().getLast_page());
                TOTAL_PAGES = response.body().getLast_page();
                if (response.body() != null) {
                    FishAd fishAd = response.body();
                    adapter.addAll(fishAd.getData());
                }


                if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;

                fishesRecyclerView.addOnScrollListener(new PaginationScrollListener(fishesLinearLayoutManager) {
                    @Override
                    protected void loadMoreItems() {
                        isLoading = true;
                        currentPage += 1;

                        loadNextPage();
                    }

                    @Override
                    public int getTotalPageCount() {
                        return TOTAL_PAGES;
                    }

                    @Override
                    public boolean isLastPage() {
                        return isLastPage;
                    }

                    @Override
                    public boolean isLoading() {
                        return isLoading;
                    }
                });
            }

            @Override
            public void onFailure(Call<FishAd> call, Throwable t) {
                adapter.clear();
            }
        });
    }

    private Call<FishAd> getFishesAll() {
        return apis.getFishesOfCity(
                CITY_ID,
                currentPage
        );
    }

    private Call<FishAd> getFishesAllWithSpecificType() {

        return apis.getFishesWithSpesificType(
                Integer.parseInt(fishTypeID),
                Integer.parseInt(MainFragment.CITY_ID),
                currentPage
        );


    }

    private void loadNextPage() {

        getFishesAll().enqueue(new Callback<FishAd>() {
            @Override
            public void onResponse(Call<FishAd> call, Response<FishAd> response) {

                adapter.removeLoadingFooter();
                isLoading = false;

                List<FishAdData> results = response.body().getData();
                adapter.addAll(results);

                if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;

            }

            @Override
            public void onFailure(Call<FishAd> call, Throwable t) {

            }
        });

    }

//    void list(){
//        FishAd fishAd = new FishAd(
//        );
//
//        fishList.add();
//
//    }

    PaginationScrollListener allListener = new PaginationScrollListener(fishesLinearLayoutManager) {
        @Override
        protected void loadMoreItems() {
            isLoading = true;
            currentPage += 1;

            loadNextPage();
        }

        @Override
        public int getTotalPageCount() {
            return TOTAL_PAGES;
        }

        @Override
        public boolean isLastPage() {
            return isLastPage;
        }

        @Override
        public boolean isLoading() {
            return isLoading;
        }
    };

    PaginationScrollListener allWithTypeListener = new PaginationScrollListener(fishesLinearLayoutManager) {
        @Override
        protected void loadMoreItems() {
            isLoading = true;
            currentPage += 1;

            loadNextPageWithSpecificType();
        }

        @Override
        public int getTotalPageCount() {
            return TOTAL_PAGES;
        }

        @Override
        public boolean isLastPage() {
            return isLastPage;
        }

        @Override
        public boolean isLoading() {
            return isLoading;
        }
    };

    @Override
    public void searchForType(FishesTypeData resul) {
        initRecyclerView();
        noOrders.setVisibility(View.GONE);
        fishTypeID = resul.getF_t_id();
        getFirstWithSpecificType();
    }
}
