package com.fekrah.hootandhwaat.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.helper.SharedHelper;
import com.fekrah.hootandhwaat.models.User;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fekrah.hootandhwaat.activities.LoginChooseActivity.LOGIN_TYPE;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.pass)
    EditText pass;

    @BindView(R.id.email)
    EditText email;
    private Apis apis;
    private String loginType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        loginType = intent.getStringExtra(LOGIN_TYPE);

        apis = BaseClient.getBaseClient().create(Apis.class);

    }

    private void logInUserClient(Apis apis, String email, String pass) {
        apis.logInClient(
                email,
                pass
        ).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.body() != null) {
                    User user = response.body();
                    if (user.getUser() != null) {
                        SharedHelper.putKey(getApplicationContext(), "log_in", "yes");
                        SharedHelper.putKey(getApplicationContext(), "log_in_type", "userClient");
                        SharedHelper.putKey(getApplicationContext(), "user_name", user.getUser().getName());
                        SharedHelper.putKey(getApplicationContext(), "user_address", user.getUser().getAddress());
                        SharedHelper.putKey(getApplicationContext(), "user_email", user.getUser().getEmail());
                        SharedHelper.putKey(getApplicationContext(), "user_image", user.getUser().getImage());
                        SharedHelper.putKey(getApplicationContext(), "user_token", user.getUser().getToken());
                        SharedHelper.putKey(getApplicationContext(), "user_mobile", user.getUser().getMobile());
                        SharedHelper.putKey(getApplicationContext(), "user_id", user.getUser().getUser_id());
                        MainActivity.hideProvider();
                        finish();

                    } else {
                        Toast.makeText(LoginActivity.this, "Wrong email or password", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(LoginActivity.this, "Wrong email or password", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Wrong email or password", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void goMain(View view) {
        if (email.getText().toString().equals("") || pass.getText().toString().equals("")) {
            Toast.makeText(this, "Enter email and password", Toast.LENGTH_SHORT).show();

        } else {
            if (loginType.equals(LoginChooseActivity.CUSTOMER))
                logInUserClient(apis, email.getText().toString(), pass.getText().toString());
            else
                loginUserProvider(apis, email.getText().toString(), pass.getText().toString());
        }
    }

    private void loginUserProvider(Apis apis, String email, String pass) {
        apis.logInProvider(
                email,
                pass
        ).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.body() != null) {
                    User user = response.body();
                    if (user.getUser() != null) {

                        SharedHelper.putKey(getApplicationContext(), "log_in", "yes");
                        SharedHelper.putKey(getApplicationContext(), "log_in_type", "userProvider");
                        SharedHelper.putKey(getApplicationContext(), "user_name", user.getUser().getName());
                        SharedHelper.putKey(getApplicationContext(), "user_address", user.getUser().getAddress());
                        SharedHelper.putKey(getApplicationContext(), "user_email", user.getUser().getEmail());
                        SharedHelper.putKey(getApplicationContext(), "user_image", user.getUser().getImage());
                        SharedHelper.putKey(getApplicationContext(), "user_token", user.getUser().getToken());
                        SharedHelper.putKey(getApplicationContext(), "user_mobile", user.getUser().getMobile());
                        SharedHelper.putKey(getApplicationContext(), "user_id", user.getUser().getCustomer_id());
                        MainActivity.hideClient();

                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, "Wrong email or password", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(LoginActivity.this, "Wrong email or password", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Wrong email or password", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void goRegister(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        intent.putExtra(LOGIN_TYPE, loginType);
        startActivity(intent);
        finish();
    }
}
