package com.fekrah.hootandhwaat.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.adapters.TripsAdapter;
import com.fekrah.hootandhwaat.helper.SharedHelper;
import com.fekrah.hootandhwaat.models.BuyResponse;
import com.fekrah.hootandhwaat.models.single_trip_ad.SingleTripAd;
import com.fekrah.hootandhwaat.models.single_trip_ad.SingleTripAdData;
import com.fekrah.hootandhwaat.models.trips.TripAdData;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;
import com.rafakob.drawme.DrawMeButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReserveTripActivity extends AppCompatActivity {

    SimpleDraweeView img;
    LinearLayout reserveView;
    DrawMeButton reserveBtn;
    TextView price;
    TextView name;
    TextView description;
    EditText tN;
    TextView period;
    TextView date;
    private TripAdData fish;
    private Dialog dialog;
    TextView servicesT;
    private String tripId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserve_trip);

        Intent intent = getIntent();

        fish = (TripAdData) intent.getSerializableExtra(TripsAdapter.TRIP_ID);

        img = findViewById(R.id.trip_img);
        name = findViewById(R.id.boat_name);
        price = findViewById(R.id.trip_price);
        reserveView = findViewById(R.id.reserveView);
        reserveBtn = findViewById(R.id.reserveBtn);
        description = findViewById(R.id.trip_description);
        period = findViewById(R.id.trip_period);
        date = findViewById(R.id.trip_date);
        tN = findViewById(R.id.t_n);
        servicesT = findViewById(R.id.services);
        price.setText(fish.getTrip_price() + getString(R.string.sr));
        img.setImageURI(fish.getTrip_image());
        name.setText(fish.getTrip_boatName());

        description.setText(fish.getPassengers() + getString(R.string.Ppassenfer));
        period.setText(fish.getTrip_time());
        date.setText(fish.getTrip_date());

        Apis apis = BaseClient.getBaseClient().create(Apis.class);

        Call<SingleTripAd> tripAdCall = apis.getSingleAd(fish.getTrip_id());
        tripAdCall.enqueue(new Callback<SingleTripAd>() {
            @Override
            public void onResponse(Call<SingleTripAd> call, Response<SingleTripAd> response) {
                if (response.body() != null && response.body().getData() != null) {
                    SingleTripAdData fish = response.body().getData();
                    price.setText(fish.getTrip_price() + getString(R.string.sr));
                    img.setImageURI(fish.getTrip_image());
                    name.setText(fish.getTrip_boatName());
                    tripId = fish.getTrip_id();
                    description.setText(fish.getPassengers() + getString(R.string.Ppassenfer));
                    period.setText(fish.getTrip_time());
                    date.setText(fish.getTrip_date());

                    if (response.body().getServices() != null) {
                        if (response.body().getServices().size() >= 1) {
                            List<String> services = response.body().getServices();
                            for (String srevice : services) {
                                servicesT.append(srevice + "\n");
                            }
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<SingleTripAd> call, Throwable t) {

            }
        });

    }

    public void reserve(View view) {

        dialog = new Dialog(this);
        dialog.show();
        Apis apis = BaseClient.getBaseClient().create(Apis.class);
//                            Call<BuyResponse> buyResponseCall = apis.buyFishes(Integer.parseInt(SharedHelper.getKey(context, "user_id")),
//                                    Integer.parseInt(fish.getF_id()));
//
//            Call<BuyResponse> buyResponseCall = apis.reserveTrip(SharedHelper.getKey(this,"user_token"),
//                    Integer.parseInt(SharedHelper.getKey(this,"user_id")),Integer.parseInt(tripId),Integer.parseInt(tN.getText().toString()));

        Call<BuyResponse> buyResponseCall = apis.reserveTrip(SharedHelper.getKey(this, "user_token"),
                Integer.parseInt(SharedHelper.getKey(this, "user_id")), Integer.parseInt(tripId), Integer.parseInt(tN.getText().toString()));


        buyResponseCall.enqueue(new Callback<BuyResponse>() {
            @Override
            public void onResponse(Call<BuyResponse> call, Response<BuyResponse> response) {
                dialog.dismiss();
                if (response.body() != null) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(ReserveTripActivity.this);

                    Log.d("oooooo", "onFailure: " + response.body().getMsg());
                    if (response.body().getCode() == 1) {
                        dialog.setTitle("لقد تم ارسال الطلب");
                    } else if (response.body().getCode() == 2) {
                        dialog.setTitle("عدد التذاكر المطلوبة اكثر من المتاح");
                    } else if (response.body().getCode() == 4) {
                        dialog.setTitle("لقد تم ارسال الطلب من قبل");
                    } else if (response.body().getCode() == 5) {
                        dialog.setTitle("لقد تم انتهاء الرحلة");
                    }
                    dialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    Log.d("oooooo", "ok: " + response.body().getMsg());
                } else {
                    Log.d("oooooo", "onFailure: null");
                }
            }

            @Override
            public void onFailure(Call<BuyResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Some thing went wrong", Toast.LENGTH_SHORT).show();
                Log.d("oooooo", "onFailure: " + t.getMessage());
            }
        });

    }
}
