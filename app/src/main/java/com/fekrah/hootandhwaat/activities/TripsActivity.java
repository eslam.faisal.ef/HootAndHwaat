package com.fekrah.hootandhwaat.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.adapters.TripsAdapter;
import com.fekrah.hootandhwaat.fragments.MainFragment;
import com.fekrah.hootandhwaat.models.trips.TripAd;
import com.fekrah.hootandhwaat.models.trips.TripAdData;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;
import com.fekrah.hootandhwaat.utility.PaginationScrollListener;
import com.john.waveview.WaveView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TripsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.fishes_recyclerview)
    RecyclerView fishesRecyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    List<TripAdData> fishList;
    TripsAdapter adapter;
    LinearLayoutManager fishesLinearLayoutManager;
    private Apis apis;
    private Dialog dialog;

    private static final int PAGE_START = 1;

    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.
    private int TOTAL_PAGES;
    private int currentPage = PAGE_START;
    private String marasiID = "1";
    private String boatId = "1";

    @BindView(R.id.sbeed)
    TextView sbeetBot;
    @BindView(R.id.yakht)
    TextView yakht;
    @BindView(R.id.qarb)
    TextView qarb;
    @BindView(R.id.dbab)
    TextView dbab;
    @BindView(R.id.marsa_motawast)
    TextView marsa_motawast;
    @BindView(R.id.marsa_ahmer)
    TextView marsa_ahmer;


    @BindView(R.id.wave_view)
    WaveView progress;

    @BindView(R.id.empty_orders)
    TextView noOrders;    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fishes);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        fishList = new ArrayList<>();
        adapter = new TripsAdapter(fishList, this);
        fishesLinearLayoutManager = new LinearLayoutManager(this);
        fishesRecyclerView.setLayoutManager(fishesLinearLayoutManager);
        fishesRecyclerView.setItemAnimator(new DefaultItemAnimator());

        fishesRecyclerView.setAdapter(adapter);

        apis = BaseClient.getBaseClient().create(Apis.class);

        dialog = new Dialog(this);
        ProgressBar progressBar = new ProgressBar(this);
        dialog.setContentView(progressBar);
        dialog.setCancelable(false);
      //  dialog.show();
        getFirstAll();

        qarb.setOnClickListener(this);
        sbeetBot.setOnClickListener(this);
        yakht.setOnClickListener(this);
        marsa_ahmer.setOnClickListener(this);
        marsa_motawast.setOnClickListener(this);
        dbab.setOnClickListener(this);

    }

    private void getFirstAll() {
        progress.setVisibility(View.VISIBLE);
        noOrders.setVisibility(View.GONE);
        adapter.clear();

        getFirstTrips().enqueue(new Callback<TripAd>() {
            @Override
            public void onResponse(Call<TripAd> call, Response<TripAd> response) {
                if (response.body()!=null&&response.body().getData().size()>=1){
                    TOTAL_PAGES = response.body().getLast_page();
                    if (response.body() != null) {
                        TripAd fishAd = response.body();
                        adapter.addAll(fishAd.getData());
                    }


                    if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                    else isLastPage = true;

                    fishesRecyclerView.addOnScrollListener(new PaginationScrollListener(fishesLinearLayoutManager) {
                        @Override
                        protected void loadMoreItems() {
                            isLoading = true;
                            currentPage += 1;

                            loadNextTrips();
                        }

                        @Override
                        public int getTotalPageCount() {
                            return TOTAL_PAGES;
                        }

                        @Override
                        public boolean isLastPage() {
                            return isLastPage;
                        }

                        @Override
                        public boolean isLoading() {
                            return isLoading;
                        }
                    });
                }else {
                    noOrders.setVisibility(View.VISIBLE);
                }
                progress.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<TripAd> call, Throwable t) {
                noOrders.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
            }
        });
    }

    private void loadNextTrips(){
        getFirstTrips().enqueue(new Callback<TripAd>() {
            @Override
            public void onResponse(Call<TripAd> call, Response<TripAd> response) {

                adapter.removeLoadingFooter();
                isLoading = false;

                if (response.body() != null) {
                    TripAd fishAd = response.body();
                    adapter.addAll(fishAd.getData());
                }

                if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;

            }

            @Override
            public void onFailure(Call<TripAd> call, Throwable t) {

            }
        });

    }

    private Call<TripAd> getFirstTrips() {
        return apis.getAllTrips(
                marasiID, boatId, MainFragment.CITY_ID, currentPage
        );
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.marsa_motawast) {
            marasiID = "1";
            selectMotawast();
            getFirstAll();
        } else if (id == R.id.marsa_ahmer) {
            marasiID = "2";
            selectAhmer();
            getFirstAll();
        } else if (id == R.id.dbab) {
            boatId = "3";
            selectDbab();
            getFirstAll();
        } else if (id == R.id.sbeed) {
            boatId = "1";
            selectSebbd();
            getFirstAll();
        } else if (id == R.id.qarb) {
            boatId = "2";
            selectQarb();
            getFirstAll();
        } else if (id == R.id.yakht) {
            boatId = "4";
            selectYakht();
            getFirstAll();
        }
    }


    void selectDbab() {
        dbab.setBackgroundColor(getResources().getColor(R.color.orange));
        yakht.setBackgroundColor(getResources().getColor(R.color.white));
        qarb.setBackgroundColor(getResources().getColor(R.color.white));
        sbeetBot.setBackgroundColor(getResources().getColor(R.color.white));
    }

    void selectYakht() {
        dbab.setBackgroundColor(getResources().getColor(R.color.white));
        yakht.setBackgroundColor(getResources().getColor(R.color.orange));
        qarb.setBackgroundColor(getResources().getColor(R.color.white));
        sbeetBot.setBackgroundColor(getResources().getColor(R.color.white));
    }

    void selectQarb() {
        dbab.setBackgroundColor(getResources().getColor(R.color.white));
        yakht.setBackgroundColor(getResources().getColor(R.color.white));
        qarb.setBackgroundColor(getResources().getColor(R.color.orange));
        sbeetBot.setBackgroundColor(getResources().getColor(R.color.white));
    }

    void selectSebbd() {
        dbab.setBackgroundColor(getResources().getColor(R.color.white));
        yakht.setBackgroundColor(getResources().getColor(R.color.white));
        qarb.setBackgroundColor(getResources().getColor(R.color.white));
        sbeetBot.setBackgroundColor(getResources().getColor(R.color.orange));
    }

    void selectMotawast() {
        marsa_motawast.setBackgroundColor(getResources().getColor(R.color.orange));
        marsa_ahmer.setBackgroundColor(getResources().getColor(R.color.white));
    }

    void selectAhmer() {
        marsa_motawast.setBackgroundColor(getResources().getColor(R.color.white));
        marsa_ahmer.setBackgroundColor(getResources().getColor(R.color.orange));
    }

}

