package com.fekrah.hootandhwaat.activities;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;

import com.fekrah.hootandhwaat.R;

public class SplashTransActivity extends AppCompatActivity {


    ImageView splash;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_trans);

        splash = findViewById(R.id.splash_first_image);


        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(SplashTransActivity.this, SplashActivity.class);
                ActivityOptions options = null;

                options = ActivityOptions.makeSceneTransitionAnimation(SplashTransActivity.this, Pair.<View, String>create(splash, "imageTransition"));

                startActivity(intent, options.toBundle());
                finish();
            }
        },500);
    }
}
