package com.fekrah.hootandhwaat.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.helper.SharedHelper;
import com.fekrah.hootandhwaat.models.Boat;
import com.fekrah.hootandhwaat.models.Marsa;
import com.fekrah.hootandhwaat.models.User;
import com.fekrah.hootandhwaat.models.vehicle.VehicleResponce;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fekrah.hootandhwaat.activities.MainActivity.hideKeyBoardFromView;

public class AddVehiclesActivity extends AppCompatActivity {

    @BindView(R.id.boat_types_spinner)
    Spinner boatsSpinner;

    @BindView(R.id.marasi_spinner)
    Spinner marasi_spinner;

    @BindView(R.id.boat_name)
    EditText boatName;


    @BindView(R.id.passangers_num)
    EditText passangers;

    @BindView(R.id.boat_image)
    TextView boatImage;


    HashMap<String, String> boatsMap = new HashMap<>();
    private List<String> boatsName;
    public static String BOAT_ID;
    public static String PERIOD_ID;
    private List<String> boatsId;

    private List<String> marsaName;
    public static String MARSA_ID;
    private List<String> marsaId;

    UCrop.Options options;
    private User user;
    private String REQUEST_FOR_PICTURE;
    private static final String PROFILE_IMAGE_REQUEST = "PROFILE_IMAGE_REQUEST";
    private Bitmap thumbBitmap = null;

    byte[] profilebyte;

    @BindView(R.id.boat_image_image)
    ImageView profile_image;


    public Apis api;
    private Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicles);
        ButterKnife.bind(this);
        hideKeyBoardFromView(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.add_vehicle));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        options = new UCrop.Options();
        options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        boatsName = new ArrayList<>();
        boatsId = new ArrayList<>();

        marsaId = new ArrayList<>();
        marsaName = new ArrayList<>();

        api = BaseClient.getBaseClient().create(Apis.class);

        getBoatsTypes();
        getMarasi();

    }


    private void getMarasi() {
        Call<Marsa> callmarasi = api.getMarasi();

        callmarasi.enqueue(new Callback<Marsa>() {
            @Override
            public void onResponse(Call<Marsa> call, Response<Marsa> response) {
                if (response.body() != null) {
                    if (response.body().getMarasi().size() > 0) {
                        for (Marsa.MarsaData marsa : response.body().getMarasi()) {
                            marsaId.add(marsa.getM_id());
                            marsaName.add(marsa.get_name());
                        }

                        ArrayAdapter adapter = new ArrayAdapter(AddVehiclesActivity.this, android.R.layout.simple_spinner_dropdown_item, marsaName);

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        marasi_spinner.setAdapter(adapter);
                        marasi_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                MARSA_ID = String.valueOf(marsaId.get(position));
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }

                }
            }

            @Override
            public void onFailure(Call<Marsa> call, Throwable t) {

            }
        });
    }

    private void getBoatsTypes() {
        Call<List<Boat>> callBoats = api.getBoatsType();

        callBoats.enqueue(new Callback<List<Boat>>() {
            @Override
            public void onResponse(Call<List<Boat>> call, Response<List<Boat>> response) {

                if (response.body() != null) {
                    if (response.body().size() > 0) {
                        for (Boat boat : response.body()) {
                            boatsId.add(boat.getId());
                            boatsName.add(boat.getName());
                        }

                        ArrayAdapter adapter = new ArrayAdapter(AddVehiclesActivity.this, android.R.layout.simple_spinner_dropdown_item, boatsName);

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        boatsSpinner.setAdapter(adapter);
                        boatsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                BOAT_ID = String.valueOf(boatsId.get(position));


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Boat>> call, Throwable t) {

            }
        });
    }


    @OnClick(R.id.boat_image)
    void getImageUri() {
        REQUEST_FOR_PICTURE = PROFILE_IMAGE_REQUEST;
        ImagePicker.create(AddVehiclesActivity.this)
                .limit(1)
                .theme(R.style.UCrop)
                .folderMode(true)
                .start();
    }



    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            //Toast.makeText(this, "", Toast.LENGTH_LONG).show();
            return;
        }
        String destinationFileName = "SAMPLE_CROPPED_IMAGE_NAME" + ".jpg";

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {

            Image image = ImagePicker.getFirstImageOrNull(data);

            Uri res_url = Uri.fromFile(new File((image.getPath())));
            //imageUri = image.getPath();
            CropImage(image, res_url);

        } else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            //  if (resultUri!=null)
            assert resultUri != null;
            bitmapCompress(resultUri);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            thumbBitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);

            if (REQUEST_FOR_PICTURE.equals(PROFILE_IMAGE_REQUEST)) {
                profilebyte = byteArrayOutputStream.toByteArray();
                profile_image.setImageURI(resultUri);
                profile_image.setVisibility(View.VISIBLE);
            }

            imageUri = resultUri;

        }

    }

    private void CropImage(Image image, Uri res_url) {
        UCrop.of(res_url, Uri.fromFile(new File(getCacheDir(), image.getName())))
                .withOptions(options)
                .start(AddVehiclesActivity.this);
    }

    //upload thumb image
//    private void uploadThumbImage(byte[] thumbByte, final StorageReference thumbFilePathRef) {
//
//        thumbFilePathRef.putBytes(thumbByte).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//            @Override
//            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
//                thumbFilePathRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//                    @Override
//                    public void onSuccess(final Uri thumbUri) {
//                        mMyProfileReference.child("user_thumb_image").setValue(thumbUri.toString());
//                        //  myProfileImage.setImageURI(thumbUri);
//                    }
//                });
//            }
//        });
//    }

    private void bitmapCompress(Uri resultUri) {
        final File thumbFilepathUri = new File(resultUri.getPath());

        try {
            thumbBitmap = new Compressor(this)
                    .setQuality(50)
                    .compressToBitmap(thumbFilepathUri);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//
//    public void fileUpload() {
//
//        //  HttpLoggingInterceptor.Logger.addLogAdapter(new AndroidLogAdapter());
//
//        File file = new File(String.valueOf(imageUri));
//        //create RequestBody instance from file
//        RequestBody requestFile = RequestBody.create(MediaType.parse("image"), file);
//
//        // MultipartBody.Part is used to send also the actual file name
//        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
//
//        Gson gson = new Gson();
//        String patientData = gson.toJson(new Vehicle(
//                "5",
//                BOAT_ID,
//                boatName.getText().toString(),
//                passangers.getText().toString(),
//                ",",
//                MARSA_ID
//        ));
//
//        RequestBody description = RequestBody.create(okhttp3.MultipartBody.FORM, patientData);
//
//        // finally, execute the request
//        Call<VehicleResponce> call = api.addVehicle(description, body);
//        call.enqueue(new Callback<VehicleResponce>() {
//            @Override
//            public void onResponse(@NonNull Call<Vehicle> call, @NonNull Response<Vehicle> response) {
//                Log.d("eeeeeee", "suc: " + response.body().getBoat_name());
//
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getImage())));
//
////                Logger.d("Response: " + response);
////
////                ResponseModel responseModel = response.body();
////
////                if(responseModel != null){
////                    EventBus.getDefault().post(new EventModel("response", responseModel.getMessage()));
////                    Logger.d("Response code " + response.code() +
////                            " Response Message: " + responseModel.getMessage());
////                } else
////                    EventBus.getDefault().post(new EventModel("response", "ResponseModel is NULL"));
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<Vehicle> call, @NonNull Throwable t) {
////
//                Log.d("eeeeeee", "onFailure: " + t.getMessage());
//            }
//        });
//    }

    //    void addVehicle() {
//
//        if (imageUri != null ||
//                MARSA_ID != null ||
//                BOAT_ID != null ||
//                !boatName.getText().toString().equals("") ||
//                !passangers.getText().toString().equals("")
//                ) {
////            // finally, execute the request
////            Call<Vehicle> call = api.addVehicle("5",
////                    BOAT_ID,
////                    boatName.getText().toString(),
////                    new File(String.valueOf(imageUri)),
////                    passangers.getText().toString(),
////                    ",",
////                    MARSA_ID
////            );
////            call.enqueue(new Callback<Vehicle>() {
////                @Override
////                public void onResponse(Call<Vehicle> call,
////                                       Response<Vehicle> response) {
////
////
////                    Log.v("Upload", "success = "+response.body().getBoat_name());
////                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse()));
////                }
////
////                @Override
////                public void onFailure(Call<Vehicle> call, Throwable t) {
////                    Log.e("Upload error:", t.getMessage());
////                }
////            });
////
////            // create upload service client
//////        FileUploadService service =
//////                ServiceGenerator.createService(FileUploadService.class);
////
//            // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
//            // use the FileUtils to get the actual file by uri
//            File file = new File(String.valueOf(imageUri));
//
//            // create RequestBody instance from file
//            RequestBody requestFile =
//                    RequestBody.create(
//                            MediaType.parse("jpg"),
//                            file
//                    );
//
////            // MultipartBody.Part is used to send also the actual file name
////            MultipartBody.Part body =
////                    MultipartBody.Part.createFormData("picture", file.getName(), requestFile);
//
//            // add another part within the multipart request
//            String descriptionString = "hello, this is description speaking";
//            RequestBody description =
//                    RequestBody.create(
//                            okhttp3.MultipartBody.FORM, descriptionString);
//
//            // finally, execute the request
//            Call<Vehicle> call = api.addVehicle(SharedHelper.getKey(getApplicationContext(), "user_id"),
//                    BOAT_ID,
//                    boatName.getText().toString(),
//                    requestFile,
//                    passangers.getText().toString(),
//                    ",",
//                    MARSA_ID
//            );
//            call.enqueue(new Callback<Vehicle>() {
//                @Override
//                public void onResponse(Call<Vehicle> call,
//                                       Response<Vehicle> response) {
//
//
//                    Log.v("Upload", "success"+response.body().getBoat_name());
//                }
//
//                @Override
//                public void onFailure(Call<Vehicle> call, Throwable t) {
//                    Log.e("Upload error:", t.getMessage());
//                }
//            });
//        }
//    }

    @OnClick(R.id.add_vehicle)
    void uploadFile() {
        if (boatName.getText().toString().equals("") ||
                passangers.getText().toString().equals("") ||
                imageUri == null) {
            return;
        }

        //  String fileUri = imageUri;
        // create upload service client
//        FileUploadService service =
//                ServiceGenerator.createService(FileUploadService.class);

        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        File file = new File(imageUri.getPath());
        // create RequestBody instance from file
//        RequestBody requestFile =
//                RequestBody.create(
//                        MediaType.parse("image"),
//                        file
//                );
//
//        // MultipartBody.Part is used to send also the actual file name
//        MultipartBody.Part body =
//                MultipartBody.Part.createFormData("picture", "uyu", requestFile);
//
//        // add another part within the multipart request
//        String descriptionString = "hello, this is description speaking";
//        Gson gson = new Gson();
//        String patientData = gson.toJson(new Vehicle(
//                "5",
//                BOAT_ID,
//                boatName.getText().toString(),
//                passangers.getText().toString(),
//                ",",
//                MARSA_ID
//        ));

        RequestBody fbody = RequestBody.create(MediaType.parse("image/*"), file);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), boatName.getText().toString());
        RequestBody passN = RequestBody.create(MediaType.parse("text/plain"), passangers.getText().toString());
        RequestBody w = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody e = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody r = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody t = RequestBody.create(MediaType.parse("text/plain"), "1");

       // RequestBody description = RequestBody.create(okhttp3.MultipartBody.FORM, patientData);
        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body2 = MultipartBody.Part.createFormData("file", "lhkhuj", fbody);

        // Call<VehicleResponce> call = api.addVehicle( name,q,w,e,r,t,body2);
        Call<VehicleResponce> call = api.addVehicle(
                SharedHelper.getKey(getApplicationContext(), "user_token"),
                Integer.parseInt(SharedHelper.getKey(getApplicationContext(), "user_id")),
                Integer.parseInt(BOAT_ID),
                name,
                passN,
                "1",
                Integer.parseInt(MARSA_ID),
                fbody);


        Log.d("oooooo", "uploadFile: " + Integer.parseInt(BOAT_ID) +
                boatName.getText().toString() +
                passangers.getText().toString() +
                "1" +
                Integer.parseInt(MARSA_ID));
        //  Call<VehicleResponce> call = api.addVehicle(description, body2);
        call.enqueue(new Callback<VehicleResponce>() {
            @Override
            public void onResponse(Call<VehicleResponce> call, Response<VehicleResponce> response) {

                if (response.body()!=null){
                    if (response.body().getError()==null){
                        boatName.setText("");
                        passangers.setText("");
                        imageUri = null;
                        profile_image.setVisibility(View.GONE);
                    }else {

                    }
                }



            }

            @Override
            public void onFailure(Call<VehicleResponce> call, Throwable t) {


            }
        });
    }
//
//    void trip() {
//        dialog.show();
//        File file = new File(imageUri.getPath());
//        RequestBody fboy;
//        fboy = RequestBody.create(MediaType.parse("image/*"), file);
//        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "5");
//        RequestBody q = RequestBody.create(MediaType.parse("text/plain"), "1");
//        RequestBody w = RequestBody.create(MediaType.parse("text/plain"), "2");
//        RequestBody e = RequestBody.create(MediaType.parse("text/plain"), "2018-9-26");
//        RequestBody r = RequestBody.create(MediaType.parse("text/plain"), "1");
//        RequestBody t = RequestBody.create(MediaType.parse("text/plain"), "05:00:00");
//
//        RequestBody y = RequestBody.create(MediaType.parse("text/plain"), "05:00:00");
//        RequestBody u = RequestBody.create(MediaType.parse("text/plain"), "2");
//        RequestBody i = RequestBody.create(MediaType.parse("text/plain"), "150");
//
//
//        // MultipartBody.Part is used to send also the actual file name
//        MultipartBody.Part body2 = MultipartBody.Part.createFormData("file", "kyvvtfhg", fboy);
//
//        Call<VehicleResponce> call = api.trip(5, 1, 2, "2018-9-26",
//                "1", "05:00:00", "05:00:00", 2, 150, fboy);
//        call.enqueue(new Callback<VehicleResponce>() {
//            @Override
//            public void onResponse(Call<VehicleResponce> call, Response<VehicleResponce> response) {
//                Logger.d("Response: " + response);
//                dialog.dismiss();
//
//                VehicleResponce responseModel = response.body();
//
//////                if (responseModel != null) {
//////                    EventBus.getDefault().post(new EventModel("response", responseModel.getBoat_name()));
//////                    Logger.d("Response code " + response.code() +
//////                            " Response Message: " + responseModel.getBoat_name());
//////                } else
//////                    EventBus.getDefault().post(new EventModel("response", "ResponseModel is NULL"));
//////
//////                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(responseModel.getImage())));
////                Log.d("ooooooo", "onResponse: " + responseModel.getCode());
////                Log.d("ooooooo", "onResponse: " + responseModel.isSuccess());
////                for (Errors e : responseModel.getError()) {
////                    Log.d("ooooooo", "onResponse: " + e.getMsg());
////
////                }
//
//                Log.d("ooooooo", "onResponse: " + responseModel.getData());
//
//
//            }
//
//            @Override
//            public void onFailure(Call<VehicleResponce> call, Throwable t) {
//                Logger.d("Exception: " + t);
//                EventBus.getDefault().post(new EventModel("response", t.getMessage()));
//                dialog.dismiss();
//                Log.d("ooooooo", "onResponse: " + t.getMessage());
//
//            }
//        });
//    }
}
