package com.fekrah.hootandhwaat.adapters.provider_trips_orders;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.helper.SharedHelper;
import com.fekrah.hootandhwaat.models.BuyResponse;
import com.fekrah.hootandhwaat.models.provider_orders.ProviderTripOrderData;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;
import com.rafakob.drawme.DrawMeButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewProviderCompleteTripsOrdersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;


    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;


    List<ProviderTripOrderData> orderDataList;
    Context context;

    public NewProviderCompleteTripsOrdersAdapter(List<ProviderTripOrderData> fishList, Context context) {
        this.orderDataList = fishList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.new_layout_completed_provider_order_trip_item, parent, false);
                viewHolder = new NewProviderCompleteTripsOrdersAdapter.TripViewHolder(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.layout_item_progress, parent, false);
                viewHolder = new NewProviderCompleteTripsOrdersAdapter.LoadingVH(viewLoading);
                break;

        }
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int position) {
        final ProviderTripOrderData fish = orderDataList.get(position);
        switch (getItemViewType(position)) {

            case ITEM:

                final TripViewHolder holder = (TripViewHolder) viewHolder;
                holder.price.setText(fish.getTrip_price() + context.getString(R.string.sr));
                holder.img.setImageURI(fish.getTrip_image());
                holder.name.setText(fish.getTrip_boatName());
                holder.description.setText(fish.getNumber_of_tickets() + context.getString(R.string.Ppassenfer));
                holder.period.setText(fish.getTrip_time());
                holder.date.setText(fish.getTrip_date());
                holder.mainView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.reserveView.setVisibility(View.VISIBLE);
                    }
                });


                break;
            case LOADING:
                final LoadingVH holder2 = (LoadingVH) viewHolder;

        }

    }



    @Override
    public int getItemCount() {
        return orderDataList.size();
    }


    class TripViewHolder extends RecyclerView.ViewHolder {

        SimpleDraweeView img;
        LinearLayout reserveView;
        DrawMeButton accept;
        DrawMeButton refuse;
        TextView price;
        TextView name;
        TextView description;
        View mainView;
        TextView period;
        TextView date;

        public TripViewHolder(@NonNull View itemView) {
            super(itemView);
            mainView = itemView;
            img = mainView.findViewById(R.id.trip_img);
            name = mainView.findViewById(R.id.boat_name);
            price = mainView.findViewById(R.id.trip_price);
            reserveView = mainView.findViewById(R.id.reserveView);
            accept = mainView.findViewById(R.id.accept);
            refuse = mainView.findViewById(R.id.refuse);
            description = mainView.findViewById(R.id.trip_description);
            period = mainView.findViewById(R.id.trip_period);
            date = mainView.findViewById(R.id.trip_date);
        }
    }

    public void clear() {
        if (orderDataList != null)
            orderDataList.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<ProviderTripOrderData> newfishList) {
        this.orderDataList.addAll(newfishList);
        notifyDataSetChanged();
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {
        private ProgressBar mProgressBar;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);

        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        orderDataList.add(new ProviderTripOrderData());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = orderDataList.size() - 1;
        ProviderTripOrderData result = getItem(position);

        if (result != null) {
            orderDataList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public ProviderTripOrderData getItem(int position) {
        return orderDataList.get(position);
    }

    void accept(final int position, String id){
        final Dialog dialog = new Dialog(context);
        dialog.show();
        Apis apis = BaseClient.getBaseClient().create(Apis.class);
//                            Call<BuyResponse> buyResponseCall = apis.buyFishes(Integer.parseInt(SharedHelper.getKey(context, "user_id")),
//                                    Integer.parseInt(fish.getF_id()));

        Call<BuyResponse> buyResponseCall = apis.acceptTrip(SharedHelper.getKey(context,"user_token"),id);
        buyResponseCall.enqueue(new Callback<BuyResponse>() {
            @Override
            public void onResponse(Call<BuyResponse> call, Response<BuyResponse> response) {
                dialog.dismiss();
                orderDataList.remove(position);
                notifyDataSetChanged();
                if (response.body() != null) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                    dialog.setTitle(response.body().getMsg());
                    Log.d("oooooo", "onFailure: "+response.body().getMsg());
                    dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }else {
                    Log.d("oooooo", "onFailure: null");
                }
            }

            @Override
            public void onFailure(Call<BuyResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(context, "Some thing went wrong", Toast.LENGTH_SHORT).show();
                Log.d("oooooo", "onFailure: "+t.getMessage());
            }
        });
    }

    private void refuse(final int position , String order_id) {
        final Dialog dialog = new Dialog(context);
        dialog.show();
        Apis apis = BaseClient.getBaseClient().create(Apis.class);
//                            Call<BuyResponse> buyResponseCall = apis.buyFishes(Integer.parseInt(SharedHelper.getKey(context, "user_id")),
//                                    Integer.parseInt(fish.getF_id()));

        Call<BuyResponse> buyResponseCall = apis.refuseTrip(SharedHelper.getKey(context,"user_token"),order_id);
        buyResponseCall.enqueue(new Callback<BuyResponse>() {
            @Override
            public void onResponse(Call<BuyResponse> call, Response<BuyResponse> response) {
                dialog.dismiss();
                orderDataList.remove(position);
                notifyDataSetChanged();
                if (response.body() != null) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                    dialog.setTitle(response.body().getMsg());
                    Log.d("oooooo", "onFailure: "+response.body().getMsg());
                    dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }else {
                    Log.d("oooooo", "onFailure: null");
                }
            }

            @Override
            public void onFailure(Call<BuyResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(context, "Some thing went wrong", Toast.LENGTH_SHORT).show();
                Log.d("oooooo", "onFailure: "+t.getMessage());
            }
        });
    }

}
