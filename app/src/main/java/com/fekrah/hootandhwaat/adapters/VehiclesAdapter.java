package com.fekrah.hootandhwaat.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.models.vehicle.VehicleData;

import java.util.List;

public class VehiclesAdapter extends RecyclerView.Adapter<VehiclesAdapter.VehivleViewHolder>  {

    Context context;

    List<VehicleData>vehicleList ;

    public VehiclesAdapter(Context context, List<VehicleData> vehicleList) {
        this.context = context;
        this.vehicleList = vehicleList;
    }

    @NonNull
    @Override
    public VehivleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new VehivleViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_my_vehicle_item,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull VehivleViewHolder holder, int i) {

        VehicleData vehicle = vehicleList.get(i);

        holder.name.setText(vehicle.getBoat_name());
        holder.passNum.setText(vehicle.getPassengers_number()+context.getString(R.string.passanger));
    }

    @Override
    public int getItemCount() {
        return vehicleList.size();
    }

    public void add(List<VehicleData> vehicles) {
        vehicleList.addAll(vehicles);
        notifyDataSetChanged();
    }

    public class VehivleViewHolder extends RecyclerView.ViewHolder{

        TextView name;
        TextView passNum;
        View mainView;

        public VehivleViewHolder(@NonNull View itemView) {
            super(itemView);
            mainView = itemView;

            name = mainView.findViewById(R.id.vehivle_name);
            passNum = mainView.findViewById(R.id.passangers);
        }
    }
}
