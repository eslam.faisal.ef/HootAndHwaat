package com.fekrah.hootandhwaat.adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.activities.FishesActivity;
import com.fekrah.hootandhwaat.helper.SharedHelper;
import com.fekrah.hootandhwaat.models.BuyResponse;
import com.fekrah.hootandhwaat.models.provider_orders.ProviderFishOrderData;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProviderFishesOrdersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;


    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;


    List<ProviderFishOrderData> fishList;
    Context context;

    public ProviderFishesOrdersAdapter(List<ProviderFishOrderData> fishList, Context context) {
        this.fishList = fishList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.layout_fished_provider_order_item, parent, false);
                viewHolder = new FishViewHolder(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.layout_item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;

        }
        return viewHolder;

    }



    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int position) {
        final ProviderFishOrderData fish = fishList.get(position);

        switch (getItemViewType(position)) {

            case ITEM:
                final FishViewHolder holder = (FishViewHolder) viewHolder;

                holder.price.setText(fish.getFish_price()+context.getString(R.string.SR));
                holder.img.setImageURI(fish.getFish_image());
                if (fish.getType()!=null)
                holder.name.setText(fish.getType());
                else holder.name.setText(FishesActivity.typeName);
                if (Integer.valueOf(fish.getFish_delevary())==1){
                    holder.delivery.setText(context.getString(R.string.available_delivery));

                }else {
                    holder.delivery.setVisibility(View.GONE);
                }

                if (Integer.valueOf(fish.getFish_cooking())==1){
                    holder.readyToEat.setText(context.getString(R.string.available_cooking));

                }else {
                    holder.readyToEat.setVisibility(View.GONE);
                }

                holder.quantity.setText(fish.getFish_weight()+context.getString(R.string.km));
                holder.mainView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.detailsView.setVisibility(View.VISIBLE);
                    }
                });



                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) viewHolder;


                break;
        }

    }

    public void clear() {
        if (fishList!=null)

            fishList.clear();

    }

    public class FishViewHolder extends RecyclerView.ViewHolder{

        SimpleDraweeView img;
        TextView price;
        TextView name;
        LinearLayout actionView;
        View mainView;
        TextView readyToEat;
        TextView delivery;
        TextView quantity;
        View detailsView;
        public FishViewHolder(@NonNull View itemView) {
            super(itemView);
            mainView = itemView;
            detailsView = mainView.findViewById(R.id.fish_details);
            img = mainView.findViewById(R.id.fish_img);
            name = mainView.findViewById(R.id.fish_name);
            price = mainView.findViewById(R.id.fish_price);
            readyToEat = mainView.findViewById(R.id.ready_eat);
            delivery = mainView.findViewById(R.id.delivery);
            quantity =mainView.findViewById(R.id.quantity);

        }
    }

    @Override
    public int getItemCount() {
        return  fishList.size();
    }

    @Override
    public int getItemViewType(int position) {

            return (position == fishList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    protected class LoadingVH extends RecyclerView.ViewHolder  {
        private ProgressBar mProgressBar;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);

        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        fishList.add(new ProviderFishOrderData());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = fishList.size() - 1;
        ProviderFishOrderData result = getItem(position);

        if (result != null) {
            fishList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public ProviderFishOrderData getItem(int position) {
        return fishList.get(position);
    }


    public void addAll(List<ProviderFishOrderData> newfishList){
        fishList.addAll(newfishList);
        notifyDataSetChanged();
    }


    void accept(final int position, String id){
        final Dialog dialog = new Dialog(context);
        dialog.show();
        Apis apis = BaseClient.getBaseClient().create(Apis.class);
//                            Call<BuyResponse> buyResponseCall = apis.buyFishes(Integer.parseInt(SharedHelper.getKey(context, "user_id")),
//                                    Integer.parseInt(fish.getF_id()));

        Call<BuyResponse> buyResponseCall = apis.acceptFish(SharedHelper.getKey(context,"user_token"),id);
        buyResponseCall.enqueue(new Callback<BuyResponse>() {
            @Override
            public void onResponse(Call<BuyResponse> call, Response<BuyResponse> response) {
                dialog.dismiss();
                fishList.remove(position);
                notifyDataSetChanged();
                if (response.body() != null) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                    dialog.setTitle(response.body().getMsg());
                    Log.d("oooooo", "onFailure: "+response.body().getMsg());
                    dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    
                    dialog.show();
                }else {
                    Log.d("oooooo", "onFailure: null");
                }
            }

            @Override
            public void onFailure(Call<BuyResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(context, "Some thing went wrong", Toast.LENGTH_SHORT).show();
                Log.d("oooooo", "onFailure: "+t.getMessage());
            }
        });
    }

    private void refuse(final int position , String order_id) {
        final Dialog dialog = new Dialog(context);
        dialog.show();
        Apis apis = BaseClient.getBaseClient().create(Apis.class);
//                            Call<BuyResponse> buyResponseCall = apis.buyFishes(Integer.parseInt(SharedHelper.getKey(context, "user_id")),
//                                    Integer.parseInt(fish.getF_id()));

        Call<BuyResponse> buyResponseCall = apis.refuseFish(SharedHelper.getKey(context,"user_token"),order_id);
        buyResponseCall.enqueue(new Callback<BuyResponse>() {
            @Override
            public void onResponse(Call<BuyResponse> call, Response<BuyResponse> response) {
                dialog.dismiss();
                fishList.remove(position);
                notifyDataSetChanged();
                if (response.body() != null) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                    dialog.setTitle(response.body().getMsg());
                    Log.d("oooooo", "onFailure: "+response.body().getMsg());
                    dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }else {
                    Log.d("oooooo", "onFailure: null");
                }
            }

            @Override
            public void onFailure(Call<BuyResponse> call, Throwable t) {

                dialog.dismiss();
                Toast.makeText(context, "Some thing went wrong", Toast.LENGTH_SHORT).show();
                Log.d("oooooo", "onFailure: "+t.getMessage());

            }
        });
    }
}
