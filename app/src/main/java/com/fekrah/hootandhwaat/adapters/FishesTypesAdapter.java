package com.fekrah.hootandhwaat.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.models.fishes.FishesTypeData;
import com.rafakob.drawme.DrawMeTextView;

import java.util.List;


public class FishesTypesAdapter extends RecyclerView.Adapter<FishesTypesAdapter.TypeViewHolder> {

    List<FishesTypeData>  dataList;
    Activity context;

    FisheTypeListener fisheTypeListener;

    public void addAll(List<FishesTypeData> fishesType) {
        dataList.addAll(fishesType);
        notifyDataSetChanged();
    }

    public interface FisheTypeListener{
        void searchForType(FishesTypeData resul);
    }

    public FishesTypesAdapter(List<FishesTypeData> dataList, Activity context, FisheTypeListener fisheTypeListener) {
        this.dataList = dataList;
        this.context = context;
        this.fisheTypeListener = fisheTypeListener;
    }

    @NonNull
    @Override
    public TypeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View viewItem = inflater.inflate(R.layout.layout_fishes_types_item, parent, false);

        return new TypeViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull TypeViewHolder holder, int i) {
        final FishesTypeData typeData = dataList.get(i);

        holder.type.setText(typeData.getF_name());
        holder.type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fisheTypeListener.searchForType(typeData);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class TypeViewHolder extends RecyclerView.ViewHolder{

        DrawMeTextView type ;

        public TypeViewHolder(@NonNull View itemView) {
            super(itemView);

            type = itemView.findViewById(R.id.fish_type);
        }
    }
}
