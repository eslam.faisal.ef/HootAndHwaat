package com.fekrah.hootandhwaat.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.activities.FishesActivity;
import com.fekrah.hootandhwaat.activities.TripsActivity;
import com.fekrah.hootandhwaat.models.City;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    @BindView(R.id.cities_spinner)
    Spinner citiesSpinner;

    View mainView;
    private List<City> citiesBody;
    private Dialog dialog;

    @OnClick(R.id.trips)
    void trips() {
        startActivity(new Intent(getActivity(), TripsActivity.class));
    }

    @OnClick(R.id.fishes)
    void fishes() {
        startActivity(new Intent(getActivity(), FishesActivity.class));
    }

    public MainFragment() {
        // Required empty public constructor
    }

    List<String> cities = new ArrayList<>();
    List<String> citiesId = new ArrayList<>();

    public static String CITY_ID ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, mainView);

        dialog = new Dialog(getActivity());
        ProgressBar progressBar = new ProgressBar(getActivity());
       // dialog.setContentView(progressBar);
        dialog.setCancelable(true);
       // dialog.show();

        Apis api = BaseClient.getBaseClient().create(Apis.class);

        getcities(api);

        return mainView;
    }

    private void getcities(Apis api) {
        if (cities!=null&&cities.size()>1){
            cities.clear();
        }
        Call<List<City>> getCiriesCall = api.getCities();

        getCiriesCall.enqueue(new Callback<List<City>>() {
            @Override
            public void onResponse(Call<List<City>> call, Response<List<City>> response) {
                if (response.body() != null) {
                    if (response.body().size() > 0) {

                        citiesBody = response.body();
                        for (City city : citiesBody) {
                            cities.add(city.getC_name());
                            citiesId.add(city.getC_id());
                        }
                        ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.layout_cities_spinner_item, cities);

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        citiesSpinner.setAdapter(adapter);
                        citiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                CITY_ID = String.valueOf(citiesBody.get(position).getC_id());
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                }
                mainView.findViewById(R.id.wave_view).setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<City>> call, Throwable t) {

            }
        });
    }

}
