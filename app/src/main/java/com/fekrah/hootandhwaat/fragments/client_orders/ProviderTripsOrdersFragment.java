package com.fekrah.hootandhwaat.fragments.client_orders;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.fragments.client_orders.trips.CompletedTripsFragment;
import com.fekrah.hootandhwaat.fragments.client_orders.trips.PendingTripsFragment;
import com.fekrah.hootandhwaat.fragments.client_orders.trips.RejectedTripsFragment;

import butterknife.ButterKnife;

public class ProviderTripsOrdersFragment extends Fragment {

    private View mainView;
    private ViewPager mViewPager;
    private SectionsPagerAdapter mSectionsPagerAdapter;

    public ProviderTripsOrdersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainView = inflater.inflate(R.layout.fragment_provider_trips_orders, container, false);
        ButterKnife.bind(this, mainView);
        return mainView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new setAdapterTask().execute();
    }

    private class setAdapterTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
            // Set up the ViewPager with the sections adapter.
            mViewPager = (ViewPager) mainView.findViewById(R.id.container3);
            //mViewPager.setAdapter();

            TabLayout tabLayout = (TabLayout) mainView.findViewById(R.id.tabs);

            mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));


            mViewPager.setAdapter(mSectionsPagerAdapter);
            mViewPager.addOnAdapterChangeListener(new ViewPager.OnAdapterChangeListener() {
                @Override
                public void onAdapterChanged(@NonNull ViewPager viewPager, @Nullable PagerAdapter pagerAdapter, @Nullable PagerAdapter pagerAdapter1) {

                }
            });
        }
    }

    public static boolean pending = false;

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            if (position == 0) {
                return new CompletedTripsFragment();
            } else if (position == 1) {
                return new RejectedTripsFragment();

            } else {
                return new PendingTripsFragment();
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }

}
