package com.fekrah.hootandhwaat.fragments.provider_fishes_orders;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.adapters.provider_fishes_adapters.NewProviderPendingFishesOrdersAdapter;
import com.fekrah.hootandhwaat.helper.SharedHelper;
import com.fekrah.hootandhwaat.models.provider_orders.ProviderFishOrder;
import com.fekrah.hootandhwaat.models.provider_orders.ProviderFishOrderData;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;
import com.fekrah.hootandhwaat.utility.PaginationScrollListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewProviderPendingFishesOrdersFragment extends Fragment {

    View mainView;

    @BindView(R.id.pending_orders_recycler_view)
    RecyclerView pending_orders_recycler_view;


    List<ProviderFishOrderData> tripList;
    NewProviderPendingFishesOrdersAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    private Apis apis;



    private static final int PAGE_START = 1;

    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES;
    private int currentPage = PAGE_START;


    @BindView(R.id.wave_view)
    RelativeLayout progress;

    @BindView(R.id.empty_orders)
    TextView noOrders;
    public NewProviderPendingFishesOrdersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainView = inflater.inflate(R.layout.fragment_provider_pending_fishes_orders, container, false);
        ButterKnife.bind(this, mainView);
        return mainView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tripList = new ArrayList<>();
        adapter = new NewProviderPendingFishesOrdersAdapter(tripList, getActivity());
        linearLayoutManager = new LinearLayoutManager(getActivity());
        pending_orders_recycler_view.setLayoutManager(linearLayoutManager);

        pending_orders_recycler_view.setAdapter(adapter);
        pending_orders_recycler_view.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        apis = BaseClient.getBaseClient().create(Apis.class);

        getFirstAll();

    }

    private void getFirstAll() {
        progress.setVisibility(View.VISIBLE);

        adapter.clear();

        getFirstTrips().enqueue(new Callback<ProviderFishOrder>() {
            @Override
            public void onResponse(Call<ProviderFishOrder> call, Response<ProviderFishOrder> response) {
                if (response.body() != null && response.body().getData().size() >= 1) {
                    TOTAL_PAGES = response.body().getLast_page();
                    if (response.body() != null) {
                        ProviderFishOrder fishAd = response.body();
                        adapter.addAll(fishAd.getData());
                    }
                    if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                    else isLastPage = true;
                } else {
                    noOrders.setVisibility(View.VISIBLE);
                }
                progress.setVisibility(View.GONE);


            }

            @Override
            public void onFailure(Call<ProviderFishOrder> call, Throwable t) {
                progress.setVisibility(View.GONE);
                noOrders.setVisibility(View.VISIBLE);
            }
        });
    }

    private void loadNextPage() {

        getFirstTrips().enqueue(new Callback<ProviderFishOrder>() {
            @Override
            public void onResponse(Call<ProviderFishOrder> call, Response<ProviderFishOrder> response) {

                adapter.removeLoadingFooter();
                isLoading = false;

                ProviderFishOrder results = response.body();
                adapter.addAll(results.getData());

                if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;

            }

            @Override
            public void onFailure(Call<ProviderFishOrder> call, Throwable t) {

            }
        });

    }

    private Call<ProviderFishOrder> getFirstTrips() {
        return apis.getPendingProviderFishesOrders(
                SharedHelper.getKey(getActivity(), "user_id"), currentPage
        );
    }

}
