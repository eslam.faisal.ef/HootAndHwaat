package com.fekrah.hootandhwaat.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fekrah.hootandhwaat.R;
import com.rafakob.drawme.DrawMeButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateAdFragment extends Fragment {


    private View mainView;

    @BindView(R.id.trips_btn)
    DrawMeButton tripsBtn;

    @BindView(R.id.fishes_btn)
    DrawMeButton fishes_btn;

    @BindView(R.id.trips_choose)
    View tripChoose;

    @BindView(R.id.fishes_choose)
    View fishes_choose;

    @BindView(R.id.fishes_view)
    View fishesView;

    @BindView(R.id.trips_view)
    View tripsView;

    public CreateAdFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainView = inflater.inflate(R.layout.fragment_create_ad, container, false);
        ButterKnife.bind(this, mainView);
        return mainView;
    }

    @OnClick(R.id.trips_btn)
    void showTrips() {
        tripChoose.setVisibility(View.VISIBLE);
        fishes_choose.setVisibility(View.INVISIBLE);
        fishesView.setVisibility(View.GONE);
        tripsView.setVisibility(View.VISIBLE);
        tripsBtn.setTypeface(tripsBtn.getTypeface(), Typeface.BOLD);
        fishes_btn.setTypeface(fishes_btn.getTypeface(), Typeface.NORMAL);
        fishes_btn.setTextColor(getActivity().getResources().getColor(R.color.gray));
        tripsBtn.setTextColor(getActivity().getResources().getColor(R.color.white));
        fishes_btn.setTextSize(16);
        tripsBtn.setTextSize(20);
    }

    @OnClick(R.id.fishes_btn)
    void showFishes() {
        fishes_choose.setVisibility(View.VISIBLE);
        tripChoose.setVisibility(View.INVISIBLE);
        fishesView.setVisibility(View.VISIBLE);
        tripsView.setVisibility(View.GONE);
        fishes_btn.setTypeface(fishes_btn.getTypeface(), Typeface.BOLD);
        tripsBtn.setTypeface(tripsBtn.getTypeface(), Typeface.NORMAL);
        tripsBtn.setTextColor(getActivity().getResources().getColor(R.color.gray));
        fishes_btn.setTextColor(getActivity().getResources().getColor(R.color.white));
        tripsBtn.setTextSize(16);
        fishes_btn.setTextSize(20);

    }

}
