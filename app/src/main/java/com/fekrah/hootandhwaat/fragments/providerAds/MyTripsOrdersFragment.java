package com.fekrah.hootandhwaat.fragments.providerAds;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.adapters.TripsAdapter;
import com.fekrah.hootandhwaat.helper.SharedHelper;
import com.fekrah.hootandhwaat.models.trips.TripAd;
import com.fekrah.hootandhwaat.models.trips.TripAdData;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;
import com.fekrah.hootandhwaat.utility.PaginationScrollListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyTripsOrdersFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private OnFragmentInteractionListener mListener;

    @BindView(R.id.trips_recyclerView)
    RecyclerView fishesRecyclerView;


    List<TripAdData> fishList;
    TripsAdapter adapter;
    LinearLayoutManager fishesLinearLayoutManager;

    private static final int PAGE_START = 1;

    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES;
    private int currentPage = PAGE_START;
    private Apis apis;

    @BindView(R.id.wave_view)
    RelativeLayout progress;

    @BindView(R.id.empty_orders)
    TextView noOrders;


    public MyTripsOrdersFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mainView = inflater.inflate(R.layout.fragment_my_trips_orders, container, false);
        ButterKnife.bind(this, mainView);


        return mainView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        fishList = new ArrayList<>();
        adapter = new TripsAdapter(fishList, getActivity());
        fishesLinearLayoutManager = new LinearLayoutManager(getActivity());
        fishesRecyclerView.setLayoutManager(fishesLinearLayoutManager);
        fishesRecyclerView.setItemAnimator(new DefaultItemAnimator());

        fishesRecyclerView.setAdapter(adapter);

        apis = BaseClient.getBaseClient().create(Apis.class);

        getFirstAll();
    }

    private void getFirstAll() {
        adapter.clear();
        progress.setVisibility(View.VISIBLE);
        getFirstTrips().enqueue(new Callback<TripAd>() {
            @Override
            public void onResponse(Call<TripAd> call, Response<TripAd> response) {
                if (response != null && response.body().getData().size() >= 1) {
                    TOTAL_PAGES = response.body().getLast_page();
                    if (response.body() != null) {
                        TripAd fishAd = response.body();
                        adapter.addAll(fishAd.getData());
                    }
                    if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                    else isLastPage = true;

                    fishesRecyclerView.addOnScrollListener(new PaginationScrollListener(fishesLinearLayoutManager) {
                        @Override
                        protected void loadMoreItems() {
                            isLoading = true;
                            currentPage += 1;

                            loadNextPage();
                        }

                        @Override
                        public int getTotalPageCount() {
                            return TOTAL_PAGES;
                        }

                        @Override
                        public boolean isLastPage() {
                            return isLastPage;
                        }

                        @Override
                        public boolean isLoading() {
                            return isLoading;
                        }
                    });
                } else {
                    noOrders.setVisibility(View.VISIBLE);
                }
                progress.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<TripAd> call, Throwable t) {
                noOrders.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
            }
        });
    }

    private void loadNextPage() {

        getFirstTrips().enqueue(new Callback<TripAd>() {
            @Override
            public void onResponse(Call<TripAd> call, Response<TripAd> response) {

                adapter.removeLoadingFooter();
                isLoading = false;

                List<TripAdData> results = response.body().getData();
                adapter.addAll(results);

                if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;

            }

            @Override
            public void onFailure(Call<TripAd> call, Throwable t) {

            }
        });

    }

    private Call<TripAd> getFirstTrips() {
        return apis.getAllTripsForUser(
                SharedHelper.getKey(getActivity(), "user_id"), currentPage
        );
    }
    //    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
