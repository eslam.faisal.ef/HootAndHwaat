package com.fekrah.hootandhwaat.fragments.providerAds;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.facebook.drawee.view.SimpleDraweeView;
import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.activities.CreateAdActivity;
import com.fekrah.hootandhwaat.helper.SharedHelper;
import com.fekrah.hootandhwaat.models.City;
import com.fekrah.hootandhwaat.models.Errors;
import com.fekrah.hootandhwaat.models.EventModel;
import com.fekrah.hootandhwaat.models.periods.Period;
import com.fekrah.hootandhwaat.models.periods.PeriodDate;
import com.fekrah.hootandhwaat.models.tripType.TripType;
import com.fekrah.hootandhwaat.models.tripType.TripTypeData;
import com.fekrah.hootandhwaat.models.vehicle.Vehicle;
import com.fekrah.hootandhwaat.models.vehicle.VehicleData;
import com.fekrah.hootandhwaat.models.vehicle.VehicleResponce;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;
import com.orhanobut.logger.Logger;
import com.yalantis.ucrop.UCrop;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class CreateTripsAdFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {


    @BindView(R.id.boat_types_spinner)
    Spinner boatsSpinner;

    @BindView(R.id.period_time_spinner)
    Spinner periodsSpinner;

    @BindView(R.id.city_spinner)
    Spinner citiesSpinner;

    @BindView(R.id.trip_type_spinner)
    Spinner tripTypeSpinner;

    @BindView(R.id.trip_img)
    SimpleDraweeView tripImage;

    @BindView(R.id.sailing_date)
    TextView sailingDate;

    @BindView(R.id.from_time)
    TextView fromTime;

    @BindView(R.id.to_time)
    TextView toTime;

    @BindView(R.id.price)
    EditText price;

    @BindView(R.id.laaf)
    CheckBox laaf;

    @BindView(R.id.food)
    CheckBox food;

    @BindView(R.id.juice)
    CheckBox juice;

    @BindView(R.id.diving)
    CheckBox diving;


    private OnFragmentInteractionListener mListener;


    public static String BOAT_ID;
    public static String PERIOD_ID;
    public static String CITY_ID;
    private String TRIP_ID;
    UCrop.Options options;
    private String REQUEST_FOR_PICTURE;
    private static final String PROFILE_IMAGE_REQUEST = "PROFILE_IMAGE_REQUEST";
    private Bitmap thumbBitmap = null;

    byte[] profilebyte;
    private Uri imageUri;
    private View mainView;
    private Apis api;
    private List<String> periodId;
    private List<String> periodName;
    private List<String> vehiclesId;
    private List<String> vehiclesName;
    private String timeTyp;

    private List<City> citiesBody;
    private List<String> citiesName;
    private List<String> citiesId;

    List<String> typeName;
    List<String> typeId;
    private ProgressDialog dialog;


    public CreateTripsAdFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainView = inflater.inflate(R.layout.fragment_create_trips_ad, container, false);
        ButterKnife.bind(this, mainView);
        return mainView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        options = new UCrop.Options();
        options.setToolbarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

        api = BaseClient.getBaseClient().create(Apis.class);
        periodId = new ArrayList<>();
        periodName = new ArrayList<>();

        vehiclesId = new ArrayList<>();
        vehiclesName = new ArrayList<>();

        citiesId = new ArrayList<>();
        citiesName = new ArrayList<>();

        typeId = new ArrayList<>();
        typeName = new ArrayList<>();
        getPeriods();

        sailingDate.setOnClickListener(this);
        fromTime.setOnClickListener(this);
        toTime.setOnClickListener(this);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        sailingDate.setText("" + year + "-" + month + "-" + dayOfMonth);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if (timeTyp.equals("from")) {
            fromTime.setText("" + hourOfDay + ":" + minute);
        } else {
            toTime.setText("" + hourOfDay + ":" + minute);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.sailing_date) {
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, 2018, 11, 32);
            dialog.show();
        } else if (v.getId() == R.id.from_time) {
            timeTyp = "from";
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(), this, hour, minute, false);
            mTimePicker.show();
        } else {
            timeTyp = "to";
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(), this, hour, minute, false);
            mTimePicker.show();
        }
    }

    private void getPeriods() {
        Call<Period> periodCall = api.getPeriods();
        periodCall.enqueue(new Callback<Period>() {
            @Override
            public void onResponse(Call<Period> call, Response<Period> response) {
                if (response.body() != null) {
                    if (response.body().getPeriods().size() > 0) {

                        for (PeriodDate period : response.body().getPeriods()) {
                            periodId.add(period.getT_id());
                            periodName.add(period.getT_time());
                        }

                        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, periodName);

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        periodsSpinner.setAdapter(adapter);
                        periodsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                PERIOD_ID = String.valueOf(periodId.get(position));
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        getMyVehicles();

                    }
                }
            }

            @Override
            public void onFailure(Call<Period> call, Throwable t) {

            }
        });
    }

    private void getMyVehicles() {
        Call<Vehicle> vehicleCall = api.getVehicleForUser(SharedHelper.getKey(getActivity(), "user_id"));
        vehicleCall.enqueue(new Callback<Vehicle>() {
            @Override
            public void onResponse(Call<Vehicle> call, Response<Vehicle> response) {

                if (response.body() != null) {
                    if (response.body().getVehicles().size() > 0) {

                        for (VehicleData period : response.body().getVehicles()) {
                            vehiclesId.add(period.getId());
                            vehiclesName.add(period.getBoat_name());
                        }

                        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, vehiclesName);

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        boatsSpinner.setAdapter(adapter);
                        boatsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                BOAT_ID = String.valueOf(vehiclesId.get(position));
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }

                    getcities();
                }
            }

            @Override
            public void onFailure(Call<Vehicle> call, Throwable t) {

            }
        });

    }

    private void getcities() {
        Call<List<City>> getCiriesCall = api.getCities();

        getCiriesCall.enqueue(new Callback<List<City>>() {
            @Override
            public void onResponse(Call<List<City>> call, Response<List<City>> response) {

                if (response.body() != null) {
                    if (response.body().size() > 0) {

                        citiesBody = response.body();
                        for (City city : citiesBody) {
                            citiesName.add(city.getC_name());
                            citiesId.add(city.getC_id());
                        }
                        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, citiesName);

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        citiesSpinner.setAdapter(adapter);
                        citiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                CITY_ID = String.valueOf(citiesId.get(position));
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                }
                getTripType();
            }

            @Override
            public void onFailure(Call<List<City>> call, Throwable t) {

            }
        });
    }

    private void getTripType() {
        Call<TripType> tripTypeCall = api.getTripsType();
        tripTypeCall.enqueue(new Callback<TripType>() {
            @Override
            public void onResponse(Call<TripType> call, Response<TripType> response) {

                if (response.body() != null) {
                    if (response.body().getTypes().size() > 0) {

                        for (TripTypeData type : response.body().getTypes()) {
                            typeName.add(type.getType());
                            typeId.add(type.getId());
                        }
                        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, typeName);

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        tripTypeSpinner.setAdapter(adapter);
                        tripTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                TRIP_ID = String.valueOf(typeId.get(position));
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                }

            }

            @Override
            public void onFailure(Call<TripType> call, Throwable t) {

            }
        });
    }

    private int servicesCount = 0;
    @OnClick(R.id.add_ad)
    void trip() {

        if (fromTime.getText().equals("") || toTime.getText().equals("") || sailingDate.getText().equals("") || imageUri == null || price.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "برجاء ادخال جميع البينات", Toast.LENGTH_SHORT).show();

            return;
        }

        dialog = new ProgressDialog(getActivity());
        dialog.setTitle("اعلان جديد");
        dialog.setMessage("جار انشاء الاعلان برجاء الانتظار..");
        dialog.show();

        if (laaf.isChecked())servicesCount+=1;
        if (food.isChecked())servicesCount+=1;
        if (diving.isChecked())servicesCount+=1;
        if (juice.isChecked())servicesCount+=1;
//        RequestBody[] servicesList = new RequestBody[servicesCount];
//        if (laaf.isChecked()){
//            servicesList[0]= RequestBody.create(MediaType.parse("text/plain"), "لعف");
//        }
//        if (food.isChecked()){
//            servicesList[1]= RequestBody.create(MediaType.parse("text/plain"), "طعام");
//        }
//        if (diving.isChecked()){
//            servicesList[2]= RequestBody.create(MediaType.parse("text/plain"), "معدات غوص");
//        }
//        if (juice.isChecked()){
//            servicesList[3]= RequestBody.create(MediaType.parse("text/plain"), "عصيرات");
//        }

        List<RequestBody> servicesList = new ArrayList<>();
                if (laaf.isChecked()){
            servicesList.add(RequestBody.create(MediaType.parse("text/plain"), "لعف"));
        }
        if (food.isChecked()){
            servicesList.add(RequestBody.create(MediaType.parse("text/plain"), "طعام"));
        }
        if (diving.isChecked()){
            servicesList.add(RequestBody.create(MediaType.parse("text/plain"), "معدات غوص"));
        }
        if (juice.isChecked()){
            servicesList.add(RequestBody.create(MediaType.parse("text/plain"), "عصيرات"));
        }

        File file = new File(imageUri.getPath());

        RequestBody[] bodyList = new RequestBody[1];


        bodyList[0] = RequestBody.create(MediaType.parse("image/jpeg"), file);

        RequestBody date = RequestBody.create(MediaType.parse("text/plain"), sailingDate.getText().toString());
        RequestBody start = RequestBody.create(MediaType.parse("text/plain"), fromTime.getText().toString());
        RequestBody end = RequestBody.create(MediaType.parse("text/plain"), toTime.getText().toString());

        File propertyImageFile = new File(imageUri.getPath());
        RequestBody propertyImage = RequestBody.create(MediaType.parse("image/*"), propertyImageFile);
        MultipartBody.Part propertyImagePart = MultipartBody.Part.createFormData("PropertyImage", String.valueOf(System.currentTimeMillis()), propertyImage);

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[1];


        //   Log.d(TAG, "requestUploadSurvey: survey image " + index + "  " + surveyModel.getPicturesList().get(index).getImagePath());
        File file2 = new File(imageUri.getPath());
        RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file2);
        surveyImagesParts[0] = MultipartBody.Part.createFormData("images[]", String.valueOf(System.currentTimeMillis()) + ".jpg", surveyBody);


        Call<VehicleResponce> call = api.trip(
                SharedHelper.getKey(getActivity(), "user_token"),
                Integer.parseInt(SharedHelper.getKey(getActivity(), "user_id")),
                Integer.parseInt(BOAT_ID), Integer.parseInt(TRIP_ID), sailingDate.getText().toString(),
                Integer.parseInt(PERIOD_ID), start, end,
                Integer.parseInt(TRIP_ID),
                Integer.parseInt(price.getText().toString()),
                surveyImagesParts,
                servicesList);

        call.enqueue(new Callback<VehicleResponce>() {
            @Override
            public void onResponse(Call<VehicleResponce> call, Response<VehicleResponce> response) {
                Logger.d("Response: " + response);
                dialog.dismiss();
                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setTitle("تم انشاء الاعلان");
                dialog.setPositiveButton(getActivity().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

                if (response.body().getError() != null)
                    for (Errors errors : response.body().getError()) {
                        Log.d("ooooooo", "onResponse: " + errors.getMsg());

                    }

            }

            @Override
            public void onFailure(Call<VehicleResponce> call, Throwable t) {
                Logger.d("Exception: " + t);
                EventBus.getDefault().post(new EventModel("response", t.getMessage()));

                Log.d("ooooooo", "onResponse: " + t.getMessage());
                dialog.dismiss();
                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setTitle("لم يتم انشاء الاعلان");
                dialog.setPositiveButton(getActivity().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }


//
//        // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    @OnClick(R.id.get_image_tv)
    void setTripImage() {
        CreateAdActivity.type = "trip";
        REQUEST_FOR_PICTURE = PROFILE_IMAGE_REQUEST;
        ImagePicker.create(getActivity())
                .limit(1)
                .theme(R.style.UCrop)
                .folderMode(true)
                .start();
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            //Toast.makeText(this, "", Toast.LENGTH_LONG).show();
            return;
        }
        String destinationFileName = "SAMPLE_CROPPED_IMAGE_NAME" + ".jpg";

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {

            Image image = ImagePicker.getFirstImageOrNull(data);

            Uri res_url = Uri.fromFile(new File((image.getPath())));
            //imageUri = image.getPath();
            CropImage(image, res_url);

        } else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            //  if (resultUri!=null)
            assert resultUri != null;
            bitmapCompress(resultUri);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            thumbBitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);

            profilebyte = byteArrayOutputStream.toByteArray();
            tripImage.setImageURI(resultUri);
            tripImage.setVisibility(View.VISIBLE);


            imageUri = resultUri;

        }

    }

    private void CropImage(Image image, Uri res_url) {
        UCrop.of(res_url, Uri.fromFile(new File(getActivity().getCacheDir(), image.getName())))
                .withOptions(options)
                .start(getActivity());
    }


    private void bitmapCompress(Uri resultUri) {
        final File thumbFilepathUri = new File(resultUri.getPath());

        try {
            thumbBitmap = new Compressor(getActivity())
                    .setQuality(50)
                    .compressToBitmap(thumbFilepathUri);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
