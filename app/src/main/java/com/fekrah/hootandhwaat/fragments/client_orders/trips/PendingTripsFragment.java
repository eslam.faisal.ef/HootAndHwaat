package com.fekrah.hootandhwaat.fragments.client_orders.trips;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.adapters.ProviderTripsOrdersAdapter;
import com.fekrah.hootandhwaat.helper.SharedHelper;
import com.fekrah.hootandhwaat.models.provider_orders.ProviderTripOrder;
import com.fekrah.hootandhwaat.models.provider_orders.ProviderTripOrderData;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;
import com.fekrah.hootandhwaat.utility.PaginationScrollListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PendingTripsFragment extends Fragment {

    View mainView;

    @BindView(R.id.pending_orders_recycler_view)
    RecyclerView pending_orders_recycler_view;


    List<ProviderTripOrderData> tripList;
    ProviderTripsOrdersAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    private Apis apis;
    @BindView(R.id.wave_view)
    RelativeLayout progress;

    @BindView(R.id.empty_orders)
    TextView noOrders;


    private static final int PAGE_START = 1;

    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES;
    private int currentPage = PAGE_START;

    public PendingTripsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainView = inflater.inflate(R.layout.fragment_pending_trips, container, false);
        ButterKnife.bind(this,mainView);
        return mainView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tripList = new ArrayList<>();
        adapter = new ProviderTripsOrdersAdapter(tripList, getActivity());
        linearLayoutManager = new LinearLayoutManager(getActivity());
        pending_orders_recycler_view.setLayoutManager(linearLayoutManager);

        pending_orders_recycler_view.setAdapter(adapter);

        apis = BaseClient.getBaseClient().create(Apis.class);

        getFirstAll();

    }

    private void getFirstAll() {
        adapter.clear();
        progress.setVisibility(View.VISIBLE);
        getFirstTrips().enqueue(new Callback<ProviderTripOrder>() {
            @Override
            public void onResponse(Call<ProviderTripOrder> call, Response<ProviderTripOrder> response) {
                if (response.body() != null && response.body().getData().size() >= 1) {
                TOTAL_PAGES = response.body().getLast_page();
                if (response.body() != null) {
                    ProviderTripOrder fishAd = response.body();
                    adapter.addAll(fishAd.getData());
                }
                Log.d("llllll", "size: " + response.body().getData().get(0).getCustomer_username());

                if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;

                pending_orders_recycler_view.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
                    @Override
                    protected void loadMoreItems() {
                        isLoading = true;
                        currentPage += 1;

                        loadNextPage();
                    }

                    @Override
                    public int getTotalPageCount() {
                        return TOTAL_PAGES;
                    }

                    @Override
                    public boolean isLastPage() {
                        return isLastPage;
                    }

                    @Override
                    public boolean isLoading() {
                        return isLoading;
                    }
                });
                } else {
                    noOrders.setVisibility(View.VISIBLE);
                }
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ProviderTripOrder> call, Throwable t) {
                Log.d("oooooo", "onFailure: " + t);
                progress.setVisibility(View.GONE);
                noOrders.setVisibility(View.VISIBLE);
            }
        });
    }
    private void loadNextPage() {

        getFirstTrips().enqueue(new Callback<ProviderTripOrder>() {
            @Override
            public void onResponse(Call<ProviderTripOrder> call, Response<ProviderTripOrder> response) {

                adapter.removeLoadingFooter();
                isLoading = false;

                ProviderTripOrder results = response.body();
                adapter.addAll(results.getData());

                if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;

            }

            @Override
            public void onFailure(Call<ProviderTripOrder> call, Throwable t) {

            }
        });

    }


    private Call<ProviderTripOrder> getFirstTrips() {
        if (SharedHelper.getKey(getActivity(),"log_in_type").equals("userProvider")) {
            return apis.getPendingProviderOrders(
                    SharedHelper.getKey(getActivity(),"user_id"),  currentPage
            );
        }else {
            return apis.getPendingClientOrders(
                    SharedHelper.getKey(getActivity(),"user_id"),  currentPage
            );
        }

    }
}
