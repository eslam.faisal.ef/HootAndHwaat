package com.fekrah.hootandhwaat.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.adapters.VehiclesAdapter;
import com.fekrah.hootandhwaat.helper.SharedHelper;
import com.fekrah.hootandhwaat.models.vehicle.Vehicle;
import com.fekrah.hootandhwaat.models.vehicle.VehicleData;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;
import com.john.waveview.WaveView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VehiclesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class VehiclesFragment extends Fragment {


    @BindView(R.id.vehicles_recycler_view)
    RecyclerView vehicles_recycler_view;

    @BindView(R.id.wave_view)
    WaveView progress;

    @BindView(R.id.empty_orders)
    TextView noOrders;

    LinearLayoutManager vehicleLinearLayoutManager;
    List<VehicleData> vehicleList;
    VehiclesAdapter adapter;


    private OnFragmentInteractionListener mListener;
    private View mainView;


    public VehiclesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainView=  inflater.inflate(R.layout.fragment_vehicles, container, false);
        ButterKnife.bind(this,mainView);
        vehicleLinearLayoutManager = new LinearLayoutManager(getActivity());
        vehicleList = new ArrayList<>();
        adapter = new VehiclesAdapter(getActivity(),vehicleList);
        vehicles_recycler_view.setLayoutManager(vehicleLinearLayoutManager);
        vehicles_recycler_view.setHasFixedSize(true);
        vehicles_recycler_view.setAdapter(adapter);
        progress.setVisibility(View.VISIBLE);

        Apis apis = BaseClient.getBaseClient().create(Apis.class);
        Call<Vehicle> vehicleCall = apis.getVehicles(SharedHelper.getKey(getActivity(),"user_id"));
        vehicleCall.enqueue(new Callback<Vehicle>() {
            @Override
            public void onResponse(Call<Vehicle> call, Response<Vehicle> response) {
                if (response.body()!=null&&response.body().getVehicles().size()>0){
                    adapter.add(response.body().getVehicles());

                } else {
                    noOrders.setVisibility(View.VISIBLE);
                }
                progress.setVisibility(View.GONE);
            }
            @Override
            public void onFailure(Call<Vehicle> call, Throwable t) {
                progress.setVisibility(View.GONE);
                noOrders.setVisibility(View.VISIBLE);
            }
        });

        return mainView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
