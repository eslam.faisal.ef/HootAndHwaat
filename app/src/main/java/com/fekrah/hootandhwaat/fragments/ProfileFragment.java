package com.fekrah.hootandhwaat.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.helper.SharedHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    SimpleDraweeView image;
    TextView name,email,phone;
    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_profile, container, false);

        image = mainView.findViewById(R.id.user_image);
        name = mainView.findViewById(R.id.user_name);
        phone = mainView.findViewById(R.id.phone);
        email = mainView.findViewById(R.id.email);
        image.setImageURI(SharedHelper.getKey(getActivity(),"user_image"));
        name.setText(SharedHelper.getKey(getActivity(),"user_name"));
        phone.setText(SharedHelper.getKey(getActivity(),"user_mobile"));
        email.setText(SharedHelper.getKey(getActivity(),"user_email"));
        return mainView;

    }

}
