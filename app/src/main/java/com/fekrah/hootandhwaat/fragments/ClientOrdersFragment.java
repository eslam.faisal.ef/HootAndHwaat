package com.fekrah.hootandhwaat.fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.fragments.providerAds.MyFishesOrdersFragment;
import com.fekrah.hootandhwaat.fragments.providerAds.MyTripsOrdersFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientOrdersFragment extends Fragment {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private View mainView;

    public ClientOrdersFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainView = inflater.inflate(R.layout.fragment_my_orders, container, false);

        new setAdapterTask().execute();
        return mainView;
    }

    private class setAdapterTask extends AsyncTask<Void,Void,Void> {
        protected Void doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
            // Set up the ViewPager with the sections adapter.
            mViewPager = (ViewPager) mainView.findViewById(R.id.container2);
            //mViewPager.setAdapter();

            TabLayout tabLayout = (TabLayout) mainView.findViewById(R.id.tabs);

            mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

            mViewPager.setAdapter(mSectionsPagerAdapter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            if (position==0){
                return new MyFishesOrdersFragment();
            }else{
                return new MyTripsOrdersFragment();
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }
    }
}
