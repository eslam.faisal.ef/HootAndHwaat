package com.fekrah.hootandhwaat.fragments.providerAds;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.facebook.drawee.view.SimpleDraweeView;
import com.fekrah.hootandhwaat.R;
import com.fekrah.hootandhwaat.activities.CreateAdActivity;
import com.fekrah.hootandhwaat.helper.SharedHelper;
import com.fekrah.hootandhwaat.models.City;
import com.fekrah.hootandhwaat.models.Errors;
import com.fekrah.hootandhwaat.models.fishes.FishesType;
import com.fekrah.hootandhwaat.models.fishes.FishesTypeData;
import com.fekrah.hootandhwaat.models.vehicle.VehicleResponce;
import com.fekrah.hootandhwaat.server.Apis;
import com.fekrah.hootandhwaat.server.BaseClient;
import com.orhanobut.logger.Logger;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
//
///**
// * A simple {@link Fragment} subclass.
// * Activities that contain this fragment must implement the
// * {@link CreateFishesAdFragment.OnFragmentInteractionListener} interface
// * to handle interaction events.
// */
public class CreateFishesAdFragment extends Fragment {

    @BindView(R.id.fishs_types_spinner)
    Spinner typesSpinner;


    @BindView(R.id.city_spinner)
    Spinner citiesSpinner;


    @BindView(R.id.fishes_weight)
    EditText weigth;

    @BindView(R.id.fish_price)
    EditText price;

    @BindView(R.id.fishes_quantity)
    EditText quantity;

    @BindView(R.id.cooking)
    CheckBox cooking;

    @BindView(R.id.delivery)
    CheckBox delivery;

    TextView date;

    List<String> fishTypesName = new ArrayList<>();
    List<String> fishTypesId = new ArrayList<>();

//    private OnFragmentInteractionListener mListener;
//
//    private FishPhotoListener fishPhotoListener;
    private String fishTypeID;

    private String REQUEST_FOR_PICTURE;
    private static final String PROFILE_IMAGE_REQUEST = "PROFILE_IMAGE_REQUEST";
    private Bitmap thumbBitmap = null;
    UCrop.Options options;
    byte[] profilebyte;
    private Uri imageUri;

    @BindView(R.id.fish_image)
    SimpleDraweeView tripImage;


    public static String CITY_ID;

    private List<City> citiesBody;
    private List<String> citiesName;
    private List<String> citiesId;

    private Apis api;
    private ProgressDialog dialog;

    public CreateFishesAdFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mainView = inflater.inflate(R.layout.fragment_create_fishes_ad, container, false);
        ButterKnife.bind(this, mainView);

        options = new UCrop.Options();
        options.setToolbarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

        api = BaseClient.getBaseClient().create(Apis.class);

        citiesId = new ArrayList<>();
        citiesName = new ArrayList<>();

        getFishesTypes();
        return mainView;
    }

    private void getcities() {
        Call<List<City>> getCiriesCall = api.getCities();

        getCiriesCall.enqueue(new Callback<List<City>>() {
            @Override
            public void onResponse(Call<List<City>> call, Response<List<City>> response) {

                if (response.body() != null) {
                    if (response.body().size() > 0) {

                        citiesBody = response.body();
                        for (City city : citiesBody) {
                            citiesName.add(city.getC_name());
                            citiesId.add(city.getC_id());
                        }
                        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, citiesName);

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        citiesSpinner.setAdapter(adapter);
                        citiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                CITY_ID = String.valueOf(citiesId.get(position));
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<List<City>> call, Throwable t) {

            }
        });
    }

    public void getFishesTypes() {


        Call<FishesType> getCiriesCall = api.getFishesType();

        getCiriesCall.enqueue(new Callback<FishesType>() {
            @Override
            public void onResponse(Call<FishesType> call, Response<FishesType> response) {
                if (response.body() != null) {

                    for (FishesTypeData type : response.body().getTypes()) {
                        fishTypesName.add(type.getF_name());
                        fishTypesId.add(type.getF_t_id());
                    }
                    final ArrayAdapter adapterSpinner = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, fishTypesName);

                    adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    typesSpinner.setAdapter(adapterSpinner);
                    typesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            fishTypeID = fishTypesId.get(position);

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                }
                getcities();
            }

            @Override
            public void onFailure(Call<FishesType> call, Throwable t) {

            }
        });

    }

    @OnClick(R.id.get_image)
    void setFishImage() {
        CreateAdActivity.type = "fish";
        REQUEST_FOR_PICTURE = PROFILE_IMAGE_REQUEST;
        ImagePicker.create(getActivity())
                .limit(1)
                .theme(R.style.UCrop)
                .folderMode(true)
                .start();
    }


    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            //Toast.makeText(this, "", Toast.LENGTH_LONG).show();
            return;
        }
        String destinationFileName = "SAMPLE_CROPPED_IMAGE_NAME" + ".jpg";

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {

            Image image = ImagePicker.getFirstImageOrNull(data);

            Uri res_url = Uri.fromFile(new File((image.getPath())));
            //imageUri = image.getPath();
            CropImage(image, res_url);

        } else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            //  if (resultUri!=null)
            assert resultUri != null;
            bitmapCompress(resultUri);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            thumbBitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);

            profilebyte = byteArrayOutputStream.toByteArray();
            tripImage.setImageURI(resultUri);
            tripImage.setVisibility(View.VISIBLE);


            imageUri = resultUri;

        }

    }

    private void CropImage(Image image, Uri res_url) {
        UCrop.of(res_url, Uri.fromFile(new File(getActivity().getCacheDir(), image.getName())))
                .withOptions(options)
                .start(getActivity());
    }

    private void bitmapCompress(Uri resultUri) {
        final File thumbFilepathUri = new File(resultUri.getPath());

        try {
            thumbBitmap = new Compressor(getActivity())
                    .setQuality(50)
                    .compressToBitmap(thumbFilepathUri);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.add_fish)
    void fish() {

        if (price.getText().equals("") || quantity.getText().equals("") || weigth.getText().equals("")) {
            Toast.makeText(getActivity(), "برجاء ادخال جميع البينات", Toast.LENGTH_SHORT).show();

            return;
        }

        dialog = new ProgressDialog(getActivity());
        dialog.setTitle("اعلان جديد");
        dialog.setMessage("جار انشاء الاعلان برجاء الانتظار..");
        dialog.show();

        File file = new File(imageUri.getPath());
        RequestBody fboy;
        fboy = RequestBody.create(MediaType.parse("image/*"), file);
        RequestBody date = RequestBody.create(MediaType.parse("text/plain"), "2018-9-26");
        RequestBody priceBody = RequestBody.create(MediaType.parse("text/plain"), price.getText().toString());
        RequestBody quaBody = RequestBody.create(MediaType.parse("text/plain"), quantity.getText().toString());
        RequestBody weightBody = RequestBody.create(MediaType.parse("text/plain"), weigth.getText().toString());

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body2 = MultipartBody.Part.createFormData("images", String.valueOf(System.currentTimeMillis()), fboy);

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[1];


        //   Log.d(TAG, "requestUploadSurvey: survey image " + index + "  " + surveyModel.getPicturesList().get(index).getImagePath());
        File file2 = new File(imageUri.getPath());
        RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file2);
        surveyImagesParts[0] = MultipartBody.Part.createFormData("images[]", String.valueOf(System.currentTimeMillis()+ ".jpg"), surveyBody);

        int coockingChecked = 0;
        int deliveryChecked = 0;
        if (cooking.isChecked()) {
            coockingChecked = 1;
        }
        if (delivery.isChecked()) {
            deliveryChecked = 1;
        }
        Call<VehicleResponce> call = api.addFish(
                SharedHelper.getKey(getActivity(), "user_token"),
                Integer.parseInt(SharedHelper.getKey(getActivity(), "user_id")),
                deliveryChecked,
                coockingChecked,
                Integer.parseInt(fishTypeID),
                priceBody,
                weightBody,
                date,
                Integer.parseInt(CITY_ID),
                quaBody,
                surveyImagesParts);
        call.enqueue(new Callback<VehicleResponce>() {
            @Override
            public void onResponse(Call<VehicleResponce> call, Response<VehicleResponce> response) {
                Logger.d("Response: " + response);
                dialog.dismiss();
                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setTitle("تم انشاء الاعلان");
                dialog.setPositiveButton(getActivity().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
                VehicleResponce responseModel = response.body();

////                if (responseModel != null) {
////                    EventBus.getDefault().post(new EventModel("response", responseModel.getBoat_name()));
////                    Logger.d("Response code " + response.code() +
////                            " Response Message: " + responseModel.getBoat_name());
////                } else
////                    EventBus.getDefault().post(new EventModel("response", "ResponseModel is NULL"));
////
////                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(responseModel.getImage())));
//                Log.d("ooooooo", "onResponse: " + responseModel.getCode());
//                Log.d("ooooooo", "onResponse: " + responseModel.isSuccess());
//                for (Errors e : responseModel.getError()) {
//                    Log.d("ooooooo", "onResponse: " + e.getMsg());
//
//                }

                Log.d("ooooooo", "onResponse: " + response.body().getCode());

                if (response.body().getError() != null)
                    for (Errors errors : response.body().getError()) {
                        Log.d("ooooooo", "onResponse: " + errors.getMsg());

                    }

            }

            @Override
            public void onFailure(Call<VehicleResponce> call, Throwable t) {

                Log.d("ooooooo", "onResponse: " + t.getMessage());
                dialog.dismiss();
                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setTitle("لم يتم انشاء الاعلان");
                dialog.setPositiveButton(getActivity().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }

//
//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(String  fish) {
//        if (fishPhotoListener != null) {
//            fishPhotoListener.setResultFish(fish);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof FishPhotoListener) {
//            fishPhotoListener = (FishPhotoListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        fishPhotoListener = null;
//    }
//
//    /**
//     * This interface must be implemented by activities that contain this
//     * fragment to allow an interaction in this fragment to be communicated
//     * to the activity and potentially other fragments contained in that
//     * activity.
//     * <p>
//     * See the Android Training lesson <a href=
//     * "http://developer.android.com/training/basics/fragments/communicating.html"
//     * >Communicating with Other Fragments</a> for more information.
//     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
//
//    public interface  FishPhotoListener{
//        void setResultFish(String fish);
//    }

}
