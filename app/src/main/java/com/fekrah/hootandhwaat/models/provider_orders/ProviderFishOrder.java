package com.fekrah.hootandhwaat.models.provider_orders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProviderFishOrder {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("last_page")
    @Expose
    private int last_page;

    @SerializedName("data")
    @Expose
    private List<ProviderFishOrderData> data;


    public ProviderFishOrder() {
    }

    public String getCode() {
        return code;
    }

    public boolean isSuccess() {
        return success;
    }

    public int getLast_page() {
        return last_page;
    }

    public List<ProviderFishOrderData> getData() {
        return data;
    }
}
